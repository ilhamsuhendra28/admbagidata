/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.enterMode = CKEDITOR.ENTER_BR;
	// config.skin = 'office2013';
	// config.filebrowserBrowseUrl = '/assistium/kcfinder/browse.php?opener=ckeditor&type=files';
	// config.filebrowserImageBrowseUrl = '/assistium/kcfinder/browse.php?opener=ckeditor&type=images';
	// config.filebrowserFlashBrowseUrl = '/assistium/kcfinder/browse.php?opener=ckeditor&type=flash';
	// config.filebrowserUploadUrl = '/assistium/kcfinder/upload.php?opener=ckeditor&type=files';
	// config.filebrowserImageUploadUrl = '/assistium/kcfinder/upload.php?opener=ckeditor&type=images';
	// config.filebrowserFlashUploadUrl = '/assistium/kcfinder/upload.php?opener=ckeditor&type=flash';
	
	// config.extraPlugins = 'lightbox';
	// config.extraAllowedContent = 'a[data-lightbox,data-title,data-lightbox-saved]';
};

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Bagi Data</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<!-- Theme style -->
	
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css" />
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	   folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/dist/css/skins/_all-skins.min.css">
	<!-- Morris chart -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/morris.js/morris.css">
	<!-- jvectormap -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/jvectormap/jquery-jvectormap.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/pace/pace.min.css">
	
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.css" />
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.css" />

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.8/sweetalert2.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/ckeditor/config.js"></script>
	<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>
	<!-- highchart -->
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/highcharts/code/highcharts.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/highcharts/code/modules/exporting.js"></script>
	<script>
		$(document).ajaxStart(function() { Pace.restart(); }); 
	</script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?php echo Yii::app()->createurl('admin'); ?>" class="logo">
				<span class="logo-mini"><b>A</b>BD</span>
				<span class="logo-lg"><b>Admin</b> Bagi Data</span>
			</a>
			
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="hidden-xs"><?php echo Yii::app()->user->name; ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<p>
										<?php echo Yii::app()->user->name; ?>
										<small>Member since 23 April 2018</small>
									</p>
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="#" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo Yii::app()->createUrl('site/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<li>
							<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
						</li>
					</ul>
				</div>
			</nav>
	</header>
  
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu" data-widget="tree">
				<li class="active"><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i>Home</a></li>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Receipt</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li class="active"><a href="<?php echo Yii::app()->createUrl('admin/receipt', array('id'=>1)); ?>"><i class="fa fa-warning"></i> In Progress</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('admin/receipt', array('id'=>2)); ?>"><i class="fa fa-check"></i> Approved</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('admin/receipt', array('id'=>3)); ?>"><i class="fa fa-slack"></i> Rejected</a></li>
					</ul>
				</li>
                <li class="treeview">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Campaign</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo Yii::app()->createUrl('campaign/index', array('id'=>'0')); ?>"><i class="fa fa-warning"></i> In Progress</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('campaign/index', array('id'=>1)); ?>"><i class="fa fa-check"></i> Approved</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('campaign/index', array('id'=>2)); ?>"><i class="fa fa-slack"></i> Rejected</a></li>
					</ul>
				</li>
                <li class="treeview">
					<a href="#">
						<i class="fa fa-dashboard"></i> <span>Promo</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo Yii::app()->createUrl('promo/index', array('id'=>'0')); ?>"><i class="fa fa-warning"></i> In Progress</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('promo/index', array('id'=>1)); ?>"><i class="fa fa-check"></i> Approved</a></li>
						<li><a href="<?php echo Yii::app()->createUrl('promo/index', array('id'=>2)); ?>"><i class="fa fa-slack"></i> Rejected</a></li>
					</ul>
				</li>
				<li class="active"><a href="<?php echo Yii::app()->createUrl('promoClicks'); ?>"><i class="fa fa-dashboard"></i>Beli Promo</a></li>
				<li class="active"><a href="<?php echo Yii::app()->createUrl('admin/listfeedback'); ?>"><i class="fa fa-user-o"></i>List Feedback</a></li>
				<li class="active"><a href="<?php echo Yii::app()->createUrl('faq/admin'); ?>"><i class="fa fa-gear"></i>CRUD FAQ</a></li>
				<li class="active"><a href="<?php echo Yii::app()->createUrl('kategoriBarang/admin'); ?>"><i class="fa fa-file"></i>CRUD Barang</a></li>
			</ul>
		</section>
	</aside>
	
	<div class="content-wrapper">
		<section class="content">
			<?php echo $content; ?>
		</section>
	</div>
  
	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<b>Version</b> 2.4.0
		</div>
			<strong>Copyright &copy; 2018 <a target="blank" href="https://bagidata.com">bagidata.com</a></strong> All rights reserved.
	</footer>
	
	<aside class="control-sidebar control-sidebar-dark">
		<div class="tab-content">
			<div class="tab-pane" id="control-sidebar-home-tab"></div>
		</div>
	</aside>

	<div class="control-sidebar-bg"></div>
</div>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/PACE/pace.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/adminlte.min.js"></script>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/dist/js/demo.js"></script>
</body>
</html>

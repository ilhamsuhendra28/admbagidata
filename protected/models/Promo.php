<?php

/**
 * This is the model class for table "promo".
 *
 * The followings are the available columns in table 'promo':
 * @property integer $id
 * @property integer $id_company
 * @property string $companyid
 * @property integer $id_category
 * @property string $title
 * @property string $content
 * @property string $point
 * @property string $cash
 * @property string $begda
 * @property string $endda
 * @property integer $max_buy
 * @property integer $total_views
 * @property integer $total_clicks
 * @property integer $total_buys
 */
class Promo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_company, id_category, max_buy, total_views, total_clicks, total_buys', 'numerical', 'integerOnly'=>true),
			array('companyid', 'length', 'max'=>8),
			array('title', 'length', 'max'=>255),
			array('point, cash', 'length', 'max'=>20),
			array('content, begda, endda, approval, approval_message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_company, companyid, id_category, title, content, point, cash, begda, endda, max_buy, total_views, total_clicks, total_buys', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'image' => array(self::HAS_MANY, 'PromoImage', ['id_promo'=>'id']),
            'category' => array(self::BELONGS_TO, 'PromoCategory', ['id_category'=>'id']),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_company' => 'Id Company',
			'companyid' => 'Companyid',
			'id_category' => 'Id Category',
			'title' => 'Title',
			'content' => 'Content',
			'point' => 'Point',
			'cash' => 'Cash',
			'begda' => 'Begda',
			'endda' => 'Endda',
			'max_buy' => 'Max Buy',
			'total_views' => 'Total Views',
			'total_clicks' => 'Total Clicks',
			'total_buys' => 'Total Buys',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_company',$this->id_company);
		$criteria->compare('companyid',$this->companyid,true);
		$criteria->compare('id_category',$this->id_category);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('point',$this->point,true);
		$criteria->compare('cash',$this->cash,true);
		$criteria->compare('begda',$this->begda,true);
		$criteria->compare('endda',$this->endda,true);
		$criteria->compare('max_buy',$this->max_buy);
		$criteria->compare('total_views',$this->total_views);
		$criteria->compare('total_clicks',$this->total_clicks);
		$criteria->compare('total_buys',$this->total_buys);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Promo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

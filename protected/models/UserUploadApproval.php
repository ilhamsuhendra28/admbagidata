<?php

/**
 * This is the model class for table "user_upload_id_approval".
 *
 * The followings are the available columns in table 'user_upload_id_approval':
 * @property integer $id
 * @property integer $id_user_upload_data
 * @property integer $id_approval
 * @property string $created_date
 * @property string $created_by
 */
class UserUploadApproval extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_upload_approval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user_upload_data, id_approval', 'numerical', 'integerOnly'=>true),
			array('created_by, updated_by', 'length', 'max'=>100),
			array('created_date, updated_date, keterangan', 'safe'),
			// array('keterangan', 'required','on'=>'rejected'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user_upload_data, keterangan, id_approval, created_date, created_by, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user_upload_data' => 'Id User Upload Data',
			'id_approval' => 'id_approval',
			'keterangan' => 'Keterangan',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user_upload_data',$this->id_user_upload_data);
		$criteria->compare('id_approval',$this->id_approval);
		$criteria->compare('keterangan',$this->keterangan);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserUploadid_approval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

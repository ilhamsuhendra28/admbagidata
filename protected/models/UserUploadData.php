<?php

/**
 * This is the model class for table "user_upload_data".
 *
 * The followings are the available columns in table 'user_upload_data':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_upload_type
 * @property string $image
 * @property integer $approved
 * @property string $point
 * @property string $cash
 */
class UserUploadData extends CActiveRecord
{
	public $total, $IN_PROGRESS, $APPROVED, $REJECTED, $keterangan;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_upload_data';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, id_user, id_upload_type, id_upload_category, image_size', 'numerical', 'integerOnly'=>true),
			array('image_name, image_type', 'length', 'max'=>255),
			array('created_by, ', 'length', 'max'=>100),
			array('point, cash', 'length', 'max'=>20),
			array('image_name', 'file', 'allowEmpty' => false,'types' => 'jpg, jpeg, png, pdf', 'maxSize'=>1024 * 1024 * 8, 'tooLarge'=>'File has to be smaller than 8MB'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_upload_type, id_upload_categori, image_name, image_type, image_size, image_path, created_date, created_by, point, cash', 'safe', 'on'=>'search'),
		);
	}
	
	protected function beforeSave(){
		if (parent::beforeSave())       
		{  
		  	if($file=CUploadedFile::getInstance($this,'uploadedFile')){
		    	$this->image_name=$file->file;
	  		}
		   return true;
		}
		return false;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_upload_type' => 'Id Upload Type',
			'id_upload_category' => 'Jenis Upload',
			'image_name' => 'File',
			'image_type' => 'Image Type',
			'image_size' => 'Image Size',
			'image_path' => 'Image Path',
			'approved' => 'Approved',
			'point' => 'Point',
			'cash' => 'Cash',
			'created_date' => 'Created Date',
			'created_by' => 'Created By'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_upload_type',$this->id_upload_type);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('point',$this->point,true);
		$criteria->compare('cash',$this->cash,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserUploadData the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "social_media".
 *
 * The followings are the available columns in table 'social_media':
 * @property integer $id
 * @property string $social_media
 * @property string $socialmediaid
 */
class SocialMedia extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'social_media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('social_media, icon', 'length', 'max'=>255),
			array('socialmediaid', 'length', 'max'=>5),
            array('begda, endda, detail', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, social_media, socialmediaid, detail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'accessToken' => array(self::HAS_ONE, 'AccessToken', ['id_social_media'=>'id'], 'condition'=>'accessToken.id_user = '.Yii::app()->user->id),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'social_media' => 'Social Media',
			'socialmediaid' => 'Socialmediaid',
			'detail' => 'Detail',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('social_media',$this->social_media,true);
		$criteria->compare('socialmediaid',$this->socialmediaid,true);
		$criteria->compare('detail',$this->detail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
//    public function getAccessToken
    
    public function scopes() {
        
        return array(
            'active'=>array(
                'condition'=>'NOW() BETWEEN begda AND endda',
            ),
            'coming'=>array(
                'condition'=>'NOW() < begda'
            )
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SocialMedia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "campaign".
 *
 * The followings are the available columns in table 'campaign':
 * @property integer $id
 * @property integer $id_user
 * @property string $title
 * @property string $subject
 * @property string $description
 * @property integer $email_blast
 * @property integer $sms_blast
 * @property integer $wa_blast
 * @property string $post_date
 * @property string $end_date
 * @property string $post_time
 * @property string $created_date
 * @property string $updated_date
 * @property integer $status
 * @property integer $total_sent
 * @property integer $is_sent
 * @property integer $approval
 * @property string $approval_message
 */
class Campaign extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'campaign';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, title, description', 'required'),
			array('id_user, email_blast, sms_blast, wa_blast, status, total_sent, is_sent, approval', 'numerical', 'integerOnly'=>true),
			array('title, subject', 'length', 'max'=>255),
			array('post_date, end_date, post_time, created_date, updated_date, approval_message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, title, subject, description, email_blast, sms_blast, wa_blast, post_date, end_date, post_time, created_date, updated_date, status, total_sent, is_sent, approval, approval_message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'campaignDetail' => array(self::HAS_MANY, 'CampaignDetail', ['id_campaign'=>'id']),
            'image' => array(self::HAS_MANY, 'CampaignImage', ['id_campaign'=>'id']),
            'user' => array(self::BELONGS_TO, 'UserEnterprise', ['id_user'=>'id']),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'title' => 'Title',
			'subject' => 'Subject',
			'description' => 'Description',
			'email_blast' => 'Email Blast',
			'sms_blast' => 'Sms Blast',
			'wa_blast' => 'Wa Blast',
			'post_date' => 'Post Date',
			'end_date' => 'End Date',
			'post_time' => 'Post Time',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
			'status' => 'Status',
			'total_sent' => 'Total Sent',
			'is_sent' => 'Is Sent',
			'approval' => 'Approval',
			'approval_message' => 'Approval Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('email_blast',$this->email_blast);
		$criteria->compare('sms_blast',$this->sms_blast);
		$criteria->compare('wa_blast',$this->wa_blast);
		$criteria->compare('post_date',$this->post_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('post_time',$this->post_time,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('total_sent',$this->total_sent);
		$criteria->compare('is_sent',$this->is_sent);
		$criteria->compare('approval',$this->approval);
		$criteria->compare('approval_message',$this->approval_message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Campaign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

<?php

/**
 * This is the model class for table "receipt_retail".
 *
 * The followings are the available columns in table 'receipt_retail':
 * @property integer $id_retail
 * @property integer $id_user_upload_data
 * @property string $nama_toko
 * @property string $tgl_pembelian
 * @property string $jam_pembelian
 * @property string $created_date
 * @property string $created_by
 */
class ReceiptRetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt_retail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_toko, tgl_pembelian, jam_pembelian, alamat_toko', 'required'),
			array('id_user_upload_data', 'numerical', 'integerOnly'=>true),
			array('nama_toko', 'length', 'max'=>255),
			array('jam_pembelian', 'length', 'max'=>50),
			array('created_by', 'length', 'max'=>100),
			array('tgl_pembelian, created_date, alamat_toko', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_retail, id_user_upload_data, nama_toko, alamat_toko, tgl_pembelian, jam_pembelian, created_date, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_retail' => 'Id Retail',
			'id_user_upload_data' => 'Id User Upload Data',
			'nama_toko' => 'Nama Toko',
			'alamat_toko' => 'Alamat Toko',
			'tgl_pembelian' => 'Tgl Pembelian',
			'jam_pembelian' => 'Jam Pembelian',
			'created_date' => 'Created Date',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_retail',$this->id_retail);
		$criteria->compare('id_user_upload_data',$this->id_user_upload_data);
		$criteria->compare('nama_toko',$this->nama_toko,true);
		$criteria->compare('alamat_toko',$this->alamat_toko,true);
		$criteria->compare('tgl_pembelian',$this->tgl_pembelian,true);
		$criteria->compare('jam_pembelian',$this->jam_pembelian,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('created_by',$this->created_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptRetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

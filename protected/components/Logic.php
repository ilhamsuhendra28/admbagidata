<?php

class Logic {
     public static function randomChar($length = 6, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        if($length > 0)
        {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars))
            {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
            }

            return $the_chars;
        }
    }
    
	public static function getEncrypt($params){
		$security = Yii::app()->getSecurityManager();
		$_params = base64_encode(json_encode($params));
		$encryptedProperty = $security->encrypt($_params, Yii::app()->getSecurityManager()->encryptionKey);
		$utf8Property = utf8_encode($encryptedProperty);
		$params = array(
			'text'=>$utf8Property,
			'key'=>rand()
		);
		
		return $params;
	}	
	
	public static function getDecrypt($text){
		$security = Yii::app()->getSecurityManager();
        $hasil = $security->decrypt(utf8_decode($text), Yii::app()->getSecurityManager()->encryptionKey);
        return json_decode(base64_decode($hasil));
	}	
	
	public static function notifRegister($email){
		$params = $email;

		$activation_url = Yii::app()->createAbsoluteUrl('site/konfirmasi',array('params'=>$email, 'random'=>Logic::randomChar()));
		$message = Yii::app()->controller->renderPartial('temp_confirm', array('activation_url' => $activation_url), array(true));

		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody($message,'text/html');
		$mailer->subject = 'Notifikasi Registrasi';
		$mailer->addTo($email);
		$mailer->setFrom('hello@bagidata.com', 'Bagi Data');
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}	
	
	public static function sendPassword($email){
		$user = User::model()->findByAttributes(array('email'=>$email));

		$activation_url = Yii::app()->createAbsoluteUrl('site/changepassword',array('params'=>$email, 'random'=>Logic::randomChar()));
		$message = Yii::app()->controller->renderPartial('temp_forgot', array('activation_url' => $activation_url), array(true));

		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody($message,'text/html');
		$mailer->subject = 'Notifikasi Forgot Password';
		$mailer->addTo($email);
		$mailer->setFrom('hello@bagidata.com', 'Bagi Data');
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}

	public static function sendFeedback($user_id, $email, $message){
		$personal = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$user_id, 'id_personal_information'=>2));
		// var_dump(nl2br($message));exit;
		Yii::import('ext.yii-mail.YiiMailMessage');
		$mailer = new YiiMailMessage;
		$mailer->setBody(nl2br($message),'text/html');
		$mailer->subject = 'Halo, User '.$personal->value.' Mengirimkan Feedback';
		$mailer->addTo('hellow@bagidata.com');
		$mailer->setFrom($email, $email);
		try {
			Yii::app()->mail->send($mailer);
			return 'success';
		} catch (Exception $e) {
			return 'gagal';
		}
	}	
	
	public static function getIndoDate($date){
		if ($date != "0000-00-00") {
			$date = date('Y-m-d', strtotime($date));

			$exDate = explode('-', $date);

			$tanggal = $exDate[2];
			$month = $exDate[1];
			$tahun = $exDate[0];

			if($month==1) $bulan = 'Januari';
			else if($month==2) $bulan = 'Februari';
			else if($month==3) $bulan = 'Maret';
			else if($month==4) $bulan = 'April';
			else if($month==5) $bulan = 'Mei';
			else if($month==6) $bulan = 'Juni';
			else if($month==7) $bulan = 'Juli';
			else if($month==8) $bulan = 'Agustus';
			else if($month==9) $bulan = 'September';
			else if($month==10) $bulan = 'Oktober';
			else if($month==11) $bulan = 'November';
			else if($month==12) $bulan = 'Desember';

			if($versi=='eng'){
				$dateEng = date_create($date);
				return date_format($dateEng,'d F Y');
			}else
				return $tanggal.' '.$bulan.' '.$tahun;
		} else {
			return $date;
		}
	}

	public static function getDataPromo($v) {
		$jmlbuy = 'SELECT COUNT(id_user) as jml FROM user_promo WHERE id_promo ='.$v.'';
		$count = Yii::app()->db->createCommand($jmlbuy);
		$data = $count->queryRow();
		return $data['jml'];
	}

	public static function getBeli($id_user,$id_promo) {
		$jmlbuy = 
		'SELECT COUNT(id_promo) as jml FROM user_promo  WHERE id_user ='.$id_user.' AND id_promo ='.$id_promo.'';
		$count = Yii::app()->db->createCommand($jmlbuy);
		$data = $count->queryRow();
		return $data['jml'];
	}
}

?>
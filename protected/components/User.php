<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $point
 * @property string $cash
 */
class User extends CActiveRecord
{
	public $nama, $new_password, $repeat_password;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, email, new_password, repeat_password', 'required'),
			array('email', 'length', 'max'=>255),
			array('email', 'email'),
			array('email', 'unique'),
			array('point, cash', 'length', 'max'=>10),
			array('new_password', 'length', 'min'=>8, 'max'=>40),
			array('repeat_password', 'compare', 'compareAttribute' => 'new_password'),
			array('id, email, password, point, cash', 'safe', 'on'=>'search'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'userPromos' => array(self::HAS_MANY, 'UserPromo', ['id_user'=>'id']),
            'photo' => array(self::HAS_ONE, 'ProfilePhoto', ['id_user'=>'id']),
            'promos' => array(self::MANY_MANY, 'Promo', 'user_promo(id_user, id_promo)'),
		);
	}
    
    public function hasPromo($id)
    {
//        get list promo this user has
        $listUserPromo = UserPromo::model()->findAllByAttributes([
            'id_user' => Yii::app()->user->id,
            'id_promo' => $id
        ]);
        
        $amountUserPromo = count($listUserPromo);
        
        if ($amountUserPromo > 0)
            return TRUE;
        else
            return FALSE;
    }
    
    public function listUserPromo($id)
    {
        $listUserPromo = UserPromo::model()->findAllByAttributes([
            'id_user' => Yii::app()->user->id,
            'id_promo' => $id
        ]);
        
        return $listUserPromo;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Full Name',
			'email' => 'Email',
			'password' => 'Password',
			'new_password' => 'Password',
			'repeat_password' => 'Confirmation Password',
			'point' => 'Point',
			'cash' => 'Cash',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('point',$this->point,true);
		$criteria->compare('cash',$this->cash,true);
		$criteria->compare('active',$this->active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
    
    public function getTotalCreditCash()
    {
        $c = new CDbCriteria();
        $c->select = 'id_user, sum(amount) amount';
        $c->condition = "id_user = :idUser AND drcr = 'credit' AND type = 'cash'";
        $c->params = [':idUser' => Yii::app()->user->id];
        $c->group = 'id_user';
        $totalCredit = Wallet::model()->find($c);      
        return $totalCredit->amount;     
    }
    
    public function getTotalDebitCash()
    {
        $c = new CDbCriteria();
        $c->select = 'id_user, sum(amount) amount';
        $c->condition = "id_user = :idUser AND drcr = 'debit' AND type = 'cash'";
        $c->params = [':idUser' => Yii::app()->user->id];
        $c->group = 'id_user';
        $totalDebit = Wallet::model()->find($c);      
        return $totalDebit->amount; 
    }
    
    public function getTotalCreditPoint()
    {
        $c = new CDbCriteria();
        $c->select = 'id_user, sum(amount) amount';
        $c->condition = "id_user = :idUser AND drcr = 'credit' AND type = 'point'";
        $c->params = [':idUser' => Yii::app()->user->id];
        $c->group = 'id_user';
        $totalCredit = Wallet::model()->find($c);      
        return $totalCredit->amount;     
    }
    
    public function getTotalDebitPoint()
    {
        $c = new CDbCriteria();
        $c->select = 'id_user, sum(amount) amount';
        $c->condition = "id_user = :idUser AND drcr = 'debit' AND type = 'point'";
        $c->params = [':idUser' => Yii::app()->user->id];
        $c->group = 'id_user';
        $totalDebit = Wallet::model()->find($c);      
        return $totalDebit->amount; 
    }
}

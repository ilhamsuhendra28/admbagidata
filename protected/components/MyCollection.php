<?php

class MyCollection {
    
    private static function isLocalhost()
    {
        $whitelist = ['127.0.0.1', "::1"];
        
        if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
			$data = true;
		}else{
			$data = false;
		}
		
		return $data;
    }
    
    public static function getImageUrl($url,$app)
    {
        $listAppLocal = ['user'=>'bagidata', 'enterprise'=>'enterprise'];
        $listApp = ['user'=>'https://app.bagidata.com', 'enterprise'=>'https://enterprise.bagidata.com'];
        if (MyCollection::isLocalhost()){
            $imageUrl = 'http://localhost/'.$listAppLocal[$app].'/'.$url;
        } else {
            $imageUrl = $listApp[$app].'/'.$url;
        }
        return $imageUrl;
    }
}
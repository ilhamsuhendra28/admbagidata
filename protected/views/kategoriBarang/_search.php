<?php
/* @var $this KategoriBarangController */
/* @var $model KategoriBarang */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_kategori_barang'); ?>
		<?php echo $form->textField($model,'id_kategori_barang'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kategori_barang'); ?>
		<?php echo $form->textField($model,'kategori_barang',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
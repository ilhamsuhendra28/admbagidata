<section class="content-header">
	<h1>
		Dashboard
		<small>CRUD Kategori Barang</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('kategoriBarang/admin'); ?>">Kelola Kategori Barang</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('kategoriBarang/update',array('id'=>$id)); ?>">Edit Kategori Barang</a></li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</div>
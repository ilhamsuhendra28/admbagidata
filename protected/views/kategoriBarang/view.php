<?php
/* @var $this KategoriBarangController */
/* @var $model KategoriBarang */

$this->breadcrumbs=array(
	'Kategori Barangs'=>array('index'),
	$model->id_kategori_barang,
);

$this->menu=array(
	array('label'=>'List KategoriBarang', 'url'=>array('index')),
	array('label'=>'Create KategoriBarang', 'url'=>array('create')),
	array('label'=>'Update KategoriBarang', 'url'=>array('update', 'id'=>$model->id_kategori_barang)),
	array('label'=>'Delete KategoriBarang', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_kategori_barang),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KategoriBarang', 'url'=>array('admin')),
);
?>

<h1>View KategoriBarang #<?php echo $model->id_kategori_barang; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_kategori_barang',
		'kategori_barang',
	),
)); ?>

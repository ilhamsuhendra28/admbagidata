<?php
/* @var $this KategoriBarangController */
/* @var $data KategoriBarang */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_kategori_barang')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_kategori_barang), array('view', 'id'=>$data->id_kategori_barang)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategori_barang')); ?>:</b>
	<?php echo CHtml::encode($data->kategori_barang); ?>
	<br />


</div>
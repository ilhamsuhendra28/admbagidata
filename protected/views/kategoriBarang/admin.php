<section class="content-header">
	<h1>
		Dashboard
		<small>CRUD Kategori Barang</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('kategoriBarang/admin'); ?>">Kelola Kategori Barang</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('kategoriBarang/create'); ?>">Tambah Kategori Barang</a><br/><br/>
					<table id="crudkbarang" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Kategori Barang</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($model as $mrow){ 
							?>
								<tr>
									<td><?php echo $mrow->kategori_barang; ?></td>
									<td style="text-align:center">
										<a href="<?php echo Yii::app()->createUrl('kategoriBarang/update', array('id'=>$mrow->id_kategori_barang)); ?>"><i class="fa fa-pencil"></i></a>
										<a href="<?php echo Yii::app()->createUrl('kategoriBarang/deletedata', array('id'=>$mrow->id_kategori_barang)); ?>"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		$('#crudkbarang').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>
<?php
/* @var $this KategoriBarangController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kategori Barangs',
);

$this->menu=array(
	array('label'=>'Create KategoriBarang', 'url'=>array('create')),
	array('label'=>'Manage KategoriBarang', 'url'=>array('admin')),
);
?>

<h1>Kategori Barangs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this KategoriBarangController */
/* @var $model KategoriBarang */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kategori-barang-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="box-body">
		<div class="form-group">    
		<?php echo $form->labelEx($model,'kategori_barang'); ?>
		<?php echo $form->textField($model,'kategori_barang',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
		<?php echo $form->error($model,'kategori_barang'); ?>
		</div>
	</div>
	
	<div class="box-footer">
		<?php echo CHtml::submitButton('Submit', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<h1>List User Buy Promo <small>ID Promo <?php echo $id ?> </small></h1>

<table>
	<tr>
		<th style="background-color: #222e61;color:#fff">User ID</th>
		<th style="background-color: #222e61;color:#fff">Username</th>
		<th style="background-color: #222e61;color:#fff">Email</th>
		<th style="background-color: #222e61;color:#fff">Nomor Handphone</th>
		<th style="background-color: #222e61;color:#fff">Jumlah Beli</th>
	</tr>
	<?php foreach($model as $row){
					$email = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>1));
					$username = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>2));
					$handphone = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>3));

				?>
					<tr>
						<td>
							<?php echo $row->id_user;?>
						</td>
						<td style="text-align:center">
							<?php
								echo $username->value;
							?>
						</td>
						<td style="text-align:center">
							<?php
								echo $email->value;
							?>
						</td>
						<td style="text-align:center">
							<?php
								echo $handphone->value;
							?>
						</td>
						<td style="text-align:center">
							<?php
								echo Logic::getBeli($row->id_user,$row->id_promo);
							?>
						</td>
					</tr>

				<?php
			}?>
</table>
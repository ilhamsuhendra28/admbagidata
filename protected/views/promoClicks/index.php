<?php
/* @var $this PromoClicksController */

$this->breadcrumbs=array(
	'Promo Clicks',
);
?>
<h1>Promotions <small>Overview </small></h1>

	<table id="promoclicks" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>
						ID 
					</th>
					<th>Promo Category</th>
					<th style="text-align:center">Value</th>
				</tr>
			</thead>
			<tbody>
					<?php 
					foreach ($model as $row) {
					?>
					<tr>
						<td>
							<?php echo $row->id ?>
						</td>
						<td>
							<a href="<?php echo Yii::app()->controller->createUrl('promoclicks/detail',['id_promo'=>$row->id]); ?>">
								<?php 
									echo $row->title;
								?></a>
						</td>
						<td style="text-align:center">
							<?php
   								echo Logic::getDataPromo($row->id)  ;
							?>
						</td>
					</tr>
				<?php }?>
			</tbody>
			
	</table>
<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('promoClicks/prints'); ?>">Export to Excel</a>
<script>
	$(function () {
		$('#promoclicks').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : true,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>

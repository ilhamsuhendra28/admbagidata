<?php
/* @var $this PromoClicksController */

$this->breadcrumbs=array(
	'Promo Clicks',
);
?>
<h1>Promotions <small>Details</small></h1>
<ol class="breadcrumb">
		<li class="active"><a href="<?php echo Yii::app()->createUrl('promoClicks/index'); ?>"><i class="fa fa-home"></i>Dashboard</a></li>
	</ol>
	<table id="promoclicks" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>User ID</th>
					<th>Username</th>
					<th>Email</th>
					<th>Nomor Handphone</th>
					<th>Jumlah Beli</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model as $row){
					$email = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>1));
					$username = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>2));
					$handphone = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$row->id_user, 'id_personal_information'=>3));

				?>
					<tr>
						<td>
							<?php echo $row->id_user;?>
						</td>
						<td>
							<?php
								echo $username->value;
							?>
						</td>
						<td>
							<?php
								echo $email->value;
							?>
						</td>
						<td>
							<?php
								echo $handphone->value;
							?>
						</td>
						<td>
							<?php
								echo Logic::getBeli($row->id_user,$row->id_promo);
							?>
						</td>
					</tr>

				<?php
			}?>
			</tbody>
	</table>
<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('promoclicks/printsdetail',['id_promo'=>$id]); ?>">Export to Excel</a>
<script>
	$(function () {
		$('#promoclicks').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : true,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>
<section class="content-header">
	<h1>
		Dashboard
		<small>List Feedback</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active" <a href="<?php echo Yii::app()->createUrl('admin/listfeedback'); ?>">List Feedback</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<table id="listfeedback" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Email</th>
								<th>Isi Feedback</th>
								<th>Send Date</th>
								<th>Send By</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($model as $mrow){ 
									$personal = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$mrow->user_id, 'id_personal_information'=>2));
									$email = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$mrow->user_id, 'id_personal_information'=>1));
							?>
								<tr>
									<td><?php echo $personal->value; ?></td>
									<td><?php echo $email->value; ?></td>
									<td><?php echo nl2br($mrow->description); ?></td>
									<td><?php echo Logic::getIndoDate(date('Y-m-d', strtotime($mrow->created_date))); ?></td>
									<td><?php echo $mrow->created_by; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

					<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('admin/printFeedback'); ?>">Export to Excel</a>

				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		$('#listfeedback').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : true,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>
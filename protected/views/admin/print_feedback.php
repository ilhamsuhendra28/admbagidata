<h1>List Feedback</h1>
<table style="border: 1px solid;">
	<thead>
		<tr style="border: 1px solid;">
			<th>Nama</th>
			<th>Email</th>
			<th>Isi Feedback</th>
			<th>Send Date</th>
			<th>Send By</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($model as $mrow){ 
				$personal = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$mrow->user_id, 'id_personal_information'=>2));
				$email = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$mrow->user_id, 'id_personal_information'=>1));
		?>
			<tr style="border: 1px solid;"> 
				<td><?php echo $personal->value; ?></td>
				<td><?php echo $email->value; ?></td>
				<td><?php echo $mrow->description; ?></td>
				<td><?php echo Logic::getIndoDate(date('Y-m-d', strtotime($mrow->created_date))); ?></td>
				<td><?php echo $mrow->created_by; ?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>
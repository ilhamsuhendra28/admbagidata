<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pk-ka-form',
	'enableAjaxValidation'=>false
)); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="error" style="display:none"></div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Identitas Penumpang</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'no_identitas'); ?>
				<?php echo $form->textField($receiptpkka,'no_identitas',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'no_identitas'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Kereta</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'nama_ka'); ?>
				<?php echo $form->textField($receiptpkka,'nama_ka',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'nama_ka'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'berangkat_dari'); ?>
				<?php echo $form->textField($receiptpkka,'berangkat_dari',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'berangkat_dari'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'tujuan_ke'); ?>
				<?php echo $form->textField($receiptpkka,'tujuan_ke',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'tujuan_ke'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'tipe_penumpang'); ?>
				<?php echo $form->textField($receiptpkka,'tipe_penumpang',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'tipe_penumpang'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'kelas_kereta'); ?>
				<?php echo $form->textField($receiptpkka,'kelas_kereta',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'kelas_kereta'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'no_gerbong'); ?>
				<?php echo $form->textField($receiptpkka,'no_gerbong',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'no_gerbong'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'no_kursi'); ?>
				<?php echo $form->textField($receiptpkka,'no_kursi',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'no_kursi'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Jadwal</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkka,'tgl_berangkat'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_berangkat',  
						'language' => 'en',  
						'model' => $receiptpkka,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkka,'tgl_berangkat'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'jam_berangkat'); ?>
				<?php echo $form->textField($receiptpkka,'jam_berangkat',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'jam_berangkat'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkka,'tgl_sampai'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_sampai',  
						'language' => 'en',  
						'model' => $receiptpkka,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkka,'tgl_sampai'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'jam_sampai'); ?>
				<?php echo $form->textField($receiptpkka,'jam_sampai',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'jam_sampai'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Tiket</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkka,'tgl_pembelian_tiket'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_pembelian_tiket',  
						'language' => 'en',  
						'model' => $receiptpkka,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkka,'tgl_pembelian_tiket'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkka,'jam_pembelian_tiket'); ?>
				<?php echo $form->textField($receiptpkka,'jam_pembelian_tiket',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkka,'jam_pembelian_tiket'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Hadiah</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<?php echo CHtml::dropDownList('hadiah', '', array('1' => 'Point', '2' => 'Cash'),array('prompt'=>'Pilih Hadiah', ' class'=>'form-control', 'onchange'=>'pilihhadiahPk($(this).val())')); ?>
		</div>
		<div class="col-md-3 tampilhadiahpk" style="display:none;">
			<?php echo CHtml::textField('hadiahvalue', '',array('placeholder'=>'Cash atau Point', 'class'=>'form-control')); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">  
				<?php echo CHtml::textField('keterangan', '',array('placeholder'=>'Keterangan', 'class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-success" onclick="saveKereta(2)"><i class="fa fa-check"></i> Approve</button>
			<button type="submit" class="btn btn-danger" onclick="saveKereta(3)"><i class="fa fa-remove"></i> Reject</button>
		</div>
	</div>
<?php $this->endWidget(); ?>

<script>
function saveKereta(id_approval_kereta){
	event.preventDefault();
	var data = new FormData($("#pk-ka-form")[0]);
	jQuery.ajax({
		type: 'POST',
		dataType: "json",
		processData: false,
		contentType: false,
		url: '<?php echo Yii::app()->createAbsoluteUrl('admin/saveReceiptKereta'); ?>id/'+<?php echo $receiptpk2->id_pk; ?>+'/approval/'+id_approval_kereta,
		data: data,
		
		beforeSend: function() {
			swal({
				title : "<div style='background:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif) no-repeat top center;height: 50px;overflow: hidden;'></div> Loading...",
				showCancelButton: false,
				showConfirmButton: false,
				allowOutsideClick: false,
				width: 400,
				padding: 100
			}).catch(swal.noop);
		},
		
		success:function(data){
			if(data.hasil == 'success'){
				window.location = "<?php echo Yii::app()->createAbsoluteUrl('admin/receipt', array('id'=>$id_approval)); ?>";
			}else{
				swal.close();
				$('.error').html(data.hasil);
				$('.error').show();
			}	
		},
	});
}	
</script>
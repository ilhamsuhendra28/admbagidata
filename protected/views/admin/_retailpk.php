<div class="row">
	<div class="col-md-12">
		<center>
			<?php if($exp[0] == 'image'){ ?>
				<img height="350" src="<?php echo 'https://app.bagidata.com'.$upload->image_path; ?>"/>
			<?php }else{ ?>
				<?php echo CHtml::link($upload->image_name, array('admin/viewpdf', 'id'=>$id), array('target'=>'_blank')); ?>
			<?php } ?>
		</center>
	</div>
</div>
<?php 
	$receiptpk2 = ReceiptPk:: model()->findByAttributes(array('id_user_upload_data'=>$id));
	if(empty($receiptpk2)){
?>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'pk-form',
		'enableAjaxValidation'=>false
	)); ?>	
			<div class="row">
				<div class="col-md-12">
					<div class="error" style="display:none"></div>
					<div class="errorSummary" style="display:none"></div>
				</div>
			</div>
			
			<div class="row">				
				<div class="col-md-6">
					<div class="form-group">    
						<?php echo $form->labelEx($receiptpk,'kode_booking'); ?>
						<?php echo $form->textField($receiptpk,'kode_booking',array('size'=>60,'maxlength'=>100, 'class'=>'form-control')); ?>
						<?php echo $form->error($receiptpk,'kode_booking'); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">    
						<?php echo $form->labelEx($receiptpk,'nama_penumpang'); ?>
						<?php echo $form->textField($receiptpk,'nama_penumpang',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<?php echo $form->error($receiptpk,'nama_penumpang'); ?>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<?php echo CHtml::submitButton('Validasi', array('class'=>'btn btn-success validasi', 'onclick'=>'addPk()')); ?>
			</div>
	<?php $this->endWidget(); ?>
<?php }else{ ?>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'pk-form',
		'enableAjaxValidation'=>false
	)); ?>		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">    
					<?php echo $form->labelEx($receiptpk2,'kode_booking'); ?>
					<?php echo $form->textField($receiptpk2,'kode_booking',array('size'=>60,'maxlength'=>100, 'class'=>'form-control', 'disabled'=>true)); ?>
					<?php echo $form->error($receiptpk2,'kode_booking'); ?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">    
					<?php echo $form->labelEx($receiptpk2,'nama_penumpang'); ?>
					<?php echo $form->textField($receiptpk2,'nama_penumpang',array('size'=>60,'maxlength'=>255, 'class'=>'form-control', 'disabled'=>true)); ?>
					<?php echo $form->error($receiptpk2,'nama_penumpang'); ?>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	
	<?php if($upload->id_upload_type == 2){ ?>
		<?php $this->renderPartial('_retailpkka', array('receiptpkka'=>$receiptpkka, 'receiptpk2'=>$receiptpk2, 'id_approval'=>$id_approval)); ?>
	<?php }else if($upload->id_upload_type == 3){ ?>
		<?php $this->renderPartial('_retailpkpesawat', array('receiptpkpesawat'=>$receiptpkpesawat, 'receiptpk2'=>$receiptpk2, 'id_approval'=>$id_approval)); ?>
	<?php } ?>
<?php } ?>

<div class="notvalid" style="display:none">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'rejected-form',
		'enableAjaxValidation'=>false
	)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">  
					<?php echo $form->labelEx($keterangan,'keterangan'); ?>
					<?php echo $form->textArea($keterangan,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
					<?php echo $form->error($keterangan,'keterangan'); ?>
				</div>
			</div>
		</div>
		
		<div class="box-footer">
			<?php echo CHtml::submitButton('Reject', array('class'=>'btn btn-danger')); ?>
		</div>
	
	<?php $this->endWidget(); ?>
</div>

<script>
function pilihhadiahPk(obj){
	if(obj != ''){
		$('.tampilhadiahpk').show();
	}else{
		$('.tampilhadiahpk').hide();
	}
}	
function addPk(){
	event.preventDefault();
	var data = new FormData($("#pk-form")[0]);
	jQuery.ajax({
		type: 'POST',
		dataType: "json",
		processData: false,
		contentType: false,
		url: '<?php echo Yii::app()->createAbsoluteUrl('admin/savePk', array('id'=>$id)); ?>',
		data: data,
		
		beforeSend: function() {
			swal({
				title : "<div style='background:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif) no-repeat top center;height: 50px;overflow: hidden;'></div> Loading...",
				showCancelButton: false,
				showConfirmButton: false,
				allowOutsideClick: false,
				width: 400,
				padding: 100
			}).catch(swal.noop);
		},
		
		success:function(data){
			if (data.hasil == 'success') {
				window.location = "<?php echo Yii::app()->createAbsoluteUrl('admin/receiptdetail',array('id'=>$id, 'kategori'=>$kategori, 'id_approval'=>$id_approval)); ?>";
			}else{
				if(data.hasil == 'notvalid'){
					swal.close();
					$('.error').hide();
					$('.errorSummary').html(data.notvalid);
					$('.errorSummary').show();
					$('.notvalid').show();
					$('.validasi').hide();
					$("#pk-form input").each(function(){
						$(this).attr("readonly", true);
					});
				}else{	
					swal.close();
					$('.error').html(data.hasil);
					$('.error').show();
				}
			}	
		}
	});
}
</script>
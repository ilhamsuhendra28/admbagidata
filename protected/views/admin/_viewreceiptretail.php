<div class="row">
	<div class="col-md-12">
		<center>
			<?php if($exp[0] == 'image'){ ?>
				<img height="350" src="<?php echo 'https://app.bagidata.com'.$tampilreceipt->image_path; ?>"/>
			<?php }else{ ?>
				<?php echo CHtml::link($tampilreceipt->image_name, array('admin/viewpdf', 'id'=>$id), array('target'=>'_blank')); ?>
			<?php } ?>
		</center>
		<br/>
		<legend>Informasi Toko</legend>
		<table>
			<tbody>
				<tr>
					<td>Nama Toko</td>
					<td> : <?php echo $toko->nama_toko; ?></td>
				</tr>
				<tr>
					<td>Alamat Toko</td>
					<td> : <?php echo $toko->alamat_toko; ?></td>
				</tr>
				<tr>
					<td>Tanggal Pembelian</td>
					<td> : <?php echo Logic::getIndodate($toko->tgl_pembelian); ?></td>
				</tr>
				<tr>
					<td>Jam Pembelian</td>
					<td> : <?php echo $toko->jam_pembelian; ?></td>
				</tr>
			</tbody>
		</table>
		<br/>
		<legend>Informasi Barang</legend>
		<table id="listbarang" class="table table-bordered table-striped">
			<thead>
				<tr>
					<td>Nama Barang</td>
					<td>Jumlah Barang</td>
					<td>Satuan Barang</td>
					<td>Harga Barang</td>
					<td>Kategori Barang</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					if(!empty($barang)){
						foreach($barang as $brow){	
							$kategoriBarang = KategoriBarang::model()->findByAttributes(array('id_kategori_barang'=>$brow->id_kategori_barang));
							$satuanBarang = SatuanBarang::model()->findByAttributes(array('id_satuan_barang'=>$brow->id_satuan_barang));						
				?>
						<tr>
							
							<td><?php echo $brow->nama_barang; ?></td>
							<td><?php echo $brow->jumlah_barang; ?></td>
							<td><?php echo $satuanBarang->satuan_barang; ?></td>
							<td> : <?php echo $brow->harga_barang; ?></td>
							<td> : <?php echo $kategoriBarang->kategori_barang; ?></td>
						</tr>
					<?php 
						} 
					}else{
					?>
						<tr><td colspan="3" style="text-align:center;">No matching records found</td></tr> 
					<?php } ?>
			</tbody>
		</table>
		<br/>
		<legend>Informasi Hadiah</legend>
		<?php if(!empty($wallet)){ ?>
			Hadiah : <?php echo $wallet->type; ?> bertambah sebanyak <?php echo $wallet->amount; ?> 
		<?php }else{ ?>	
			Hadiah : Tidak ada hadiah.
		<?php } ?>
		<br/>
		<br/>
		<legend>Informasi Keterangan</legend>
		<?php if($approval->keterangan != ''){ ?>
			Keterangan : <?php echo $approval->keterangan; ?>
		<?php }else{ ?>	
			Keterangan : Tidak ada keterangan.
		<?php } ?>
	</div>
</div>

<script>
	$(function () {
		$('#listbarang').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
	})
</script>
<?php
	if($id_approval == 1){
		$tampil = 'In Progress';
	}else if($id_approval == 2){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Receipt - <?php echo $tampil; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="<?php echo Yii::app()->createUrl('admin/receipt', array('id'=>$id_approval)); ?>">Control Panel List Receipt - <?php echo $tampil; ?></a></li>
		<li class="active"><?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<?php if($id_approval == 2){ ?>
						<?php if($tampilreceipt->id_upload_type == 1){ ?>
							<?php $this->renderPartial('_viewreceiptretail', array('toko'=>$toko, 'approval'=>$approval, 'tampilreceipt'=>$tampilreceipt, 'exp'=>$exp, 'barang'=>$barang, 'wallet'=>$wallet)); ?>
						<?php }else if($tampilreceipt->id_upload_type == 2){ ?>
							<?php $this->renderPartial('_viewreceiptkereta', array('kereta'=>$kereta, 'approval'=>$approval, 'tampilreceipt'=>$tampilreceipt, 'exp'=>$exp, 'keretadetail'=>$keretadetail, 'wallet'=>$wallet)); ?>
						<?php }else if($tampilreceipt->id_upload_type == 3){ ?>
							<?php $this->renderPartial('_viewreceiptpesawat', array('pesawat'=>$pesawat, 'approval'=>$approval, 'tampilreceipt'=>$tampilreceipt, 'exp'=>$exp, 'pesawatdetail'=>$pesawatdetail, 'wallet'=>$wallet)); ?>
						<?php } ?>
					<?php }else if($id_approval == 3){ ?>
						<?php if(!empty($approval)){ ?>
							<center>
								<?php if($exp[0] == 'image'){ ?>
									<img height="350" src="<?php echo 'https://app.bagidata.com'.$tampilreceipt->image_path; ?>"/>
								<?php }else{ ?>
									<?php echo CHtml::link($tampilreceipt->image_name, array('admin/viewpdf', 'id'=>$id), array('target'=>'_blank')); ?>
								<?php } ?>
								<br/>
								Keterangan : <?php  echo $approval->keterangan != '' ? $approval->keterangan : 'Tidak ada keterangan'; ?>
							</center>
						<?php }else{ ?>
							Keterangan : Tidak ada keterangan
						<?php } ?>
					<?php } ?>	
				</div>
			</div>
		</div>
	</div>
</section>
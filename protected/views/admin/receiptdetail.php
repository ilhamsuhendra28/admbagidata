<?php
	if($id_approval == 1){
		$tampil = 'In Progress';
	}else if($id_approval == 2){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Receipt - <?php echo $tampil.' - '.$kategori; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="<?php echo Yii::app()->createUrl('admin/receipt', array('id'=>$id_approval)); ?>">Control Panel List Receipt - <?php echo $tampil; ?></a></li>
		<li class="active"><?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<?php if($kategori == 'rejected'){ ?>
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'rejected-form',
							'enableAjaxValidation'=>false
						)); ?>
							
							<div class="form-group">  
								<?php echo $form->labelEx($keterangan,'keterangan'); ?>
								<?php echo $form->textArea($keterangan,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
								<?php echo $form->error($keterangan,'keterangan'); ?>
							</div>
							
							<div class="box-footer">
								<?php echo CHtml::submitButton('Reject', array('class'=>'btn btn-danger')); ?>
							</div>

						<?php $this->endWidget(); ?>
					<?php }else{ ?>
						<div class="form">
							<?php if($upload->id_upload_type == 1){ ?>
								
								<?php $this->renderPartial('_retail', array('id'=>$id, 'kategori'=>$kategori, 'id_approval'=>$id_approval, 'receipt'=>$receipt, 'keterangan'=>$keterangan, 'exp'=>$exp, 'upload'=>$upload)); ?>
								
							<?php }else if($upload->id_upload_type == 2 || $upload->id_upload_type == 3){ ?>
								<?php $this->renderPartial('_retailpk', array('id'=>$id, 'kategori'=>$kategori, 'id_approval'=>$id_approval, 'receiptpk'=>$receiptpk, 'keterangan'=>$keterangan, 'receiptpkka'=>$receiptpkka, 'upload'=>$upload, 'receiptpkpesawat'=>$receiptpkpesawat, 'exp'=>$exp)); ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
			<div>
		</div>
	</div>
</section>
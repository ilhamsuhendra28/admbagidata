<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel Show Referral</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Control Panel Show Referral></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-3 col-xs-6">
          	<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><?php echo $jml_user; ?></h3>

					<p>User Registrations</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
			</div>
        </div>
	</div>
</section>
<?php 
	if($id == 1){
		$tampil = 'Jumlah User Yang Aktif';
	}else if($id == 2){
		$tampil = 'Jumlah User Yang Connect ke Facebook';
	}else if($id == 3){
		$tampil = 'Jumlah User Yang Sudah Upload Receipt';
	}else if($id == 4){
		$tampil = 'Jumlah Receipt Retail Yang Diupload';
	}else if($id == 5){
		$tampil = 'Jumlah Receipt Flight Yang Diupload';
	}else if($id == 6){
		$tampil = 'Jumlah Receipt Train Yang Diupload';
	}				
?>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel Detail - <?php echo $tampil; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Control Panel Detail - <?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<?php if($id == 1 || $id == 2 || $id == 3){ ?>	
						<table id="userbaris1" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Nama</th>
									<th>Email</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									if(!empty($model)){
										foreach($model as $mrow){
										$personal = UserPersonalInformation::model()->findByAttributes(array('id_user'=>$mrow->id, 'id_personal_information'=>2));										
								?>
										<tr>
											<?php if($id == 1){ ?>
												<td><?php echo $personal->value; ?></td>
											<?php }else{ ?>
												<td><?php echo $mrow->nama; ?></td>
											<?php } ?>
											<td><?php echo $mrow->email; ?></td>
											<td><?php echo $mrow->active == 1 ? "Aktif" : "Tidak Aktif" ?></td>
										</tr>
									<?php 
										} 
									}else{
									?>
										<tr><td colspan="3" style="text-align:center;">No matching records found</td></tr> 
									<?php } ?>
							</tbody>
						</table>
					<?php }else if($id == 4 || $id == 5 || $id == 6){ ?>
						<table id="userbaris2" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>In Progress</th>
									<th>Completed</th>
									<th>Rejected</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php if(!empty($model)){ ?>
										<td><?php echo $model->IN_PROGRESS; ?></td>
										<td><?php echo $model->APPROVED; ?></td>
										<td><?php echo $model->REJECTED; ?></td>
									<?php }else{ ?>
										<tr><td colspan="3" style="text-align:center;">No matching records found</td></tr>
									<?php } ?>
								</tr>
							</tbody>
						</table>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		$('#userbaris1').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
		
		$('#userbaris2').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>
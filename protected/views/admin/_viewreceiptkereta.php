<div class="row">
	<div class="col-md-12">
		<center>
			<?php if($exp[0] == 'image'){ ?>
				<img height="350" src="<?php echo 'https://app.bagidata.com'.$tampilreceipt->image_path; ?>"/>
			<?php }else{ ?>
				<?php echo CHtml::link($tampilreceipt->image_name, array('admin/viewpdf', 'id'=>$id), array('target'=>'_blank')); ?>
			<?php } ?>
		</center>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		<legend>Informasi Penumpang</legend>
		<table>
			<tbody>
				<tr>
					<td>Kode Booking</td>
					<td> : <?php echo $kereta->kode_booking; ?></td>
				</tr>
				<tr>
					<td>No Identitas</td>
					<td> : <?php echo $keretadetail->no_identitas; ?></td>
				</tr>
				<tr>
					<td>Nama Penumpang</td>
					<td> : <?php echo $kereta->nama_penumpang; ?></td>
				</tr>
				<tr>
					<td>Tipe Penumpang</td>
					<td> : <?php echo $keretadetail->tipe_penumpang; ?></td>
				</tr>
				<tr>
					<td>Cek Akun</td>
					<td> : 
						<?php 
							if($kereta->cek_akun == 1){
								echo 'Nama Penumpang sama dengan Nama Akun terdaftar';
							}else{
								echo 'Nama Penumpang tidak sama dengan Nama Akun terdaftar';
							}	
						?>
					</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<legend>Informasi Keberangkatan</legend>
		<table>
			<tbody>
				<tr>
					<td>Nama Kereta Api</td>
					<td> : <?php echo $keretadetail->nama_ka; ?></td>
				</tr>
				<tr>
					<td>Kelas Kereta</td>
					<td> : <?php echo $keretadetail->kelas_kereta; ?></td>
				</tr>
				<tr>
					<td>Berangkat Dari</td>
					<td> : <?php echo $keretadetail->berangkat_dari; ?></td>
				</tr>
				<tr>
					<td>Tanggal Keberangkatan</td>
					<td> : <?php echo Logic::getIndodate($keretadetail->tgl_berangkat); ?></td>
				</tr>
				<tr>
					<td>Jam Keberangkatan</td>
					<td> : <?php echo $keretadetail->jam_berangkat; ?></td>
				</tr>
				<tr>
					<td>Tujuan Ke</td>
					<td> : <?php echo $keretadetail->tujuan_ke; ?></td>
				</tr>
				<tr>
					<td>Tanggal Sampai</td>
					<td> : <?php echo Logic::getIndodate($keretadetail->tgl_sampai); ?></td>
				</tr>
				<tr>
					<td>Jam Sampai</td>
					<td> : <?php echo $keretadetail->jam_sampai; ?></td>
				</tr>
				<tr>
					<td>No Gerbong</td>
					<td> : <?php echo $keretadetail->no_gerbong; ?></td>
				</tr>
				<tr>
					<td>No Kursi</td>
					<td> : <?php echo $keretadetail->no_kursi; ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-6">
		<legend>Informasi Tiket</legend>
		<table>
			<tbody>
				<tr>
					<td>Tanggal Pembelian Tiket</td>
					<td> : <?php echo Logic::getIndodate($keretadetail->tgl_pembelian_tiket); ?></td>
				</tr>
				<tr>
					<td>Jam Pembelian Tiket</td>
					<td> : <?php echo $keretadetail->jam_pembelian_tiket; ?></td>
				</tr>
			</tbody>
		</table>
		<br/>
		<legend>Informasi Hadiah</legend>
		<?php if(!empty($wallet)){ ?>
			Hadiah : <?php echo $wallet->type; ?> bertambah sebanyak <?php echo $wallet->amount; ?> 
		<?php }else{ ?>	
			Hadiah : Tidak ada hadiah.
		<?php } ?>
		<br/>
		<br/>
		<legend>Informasi Keterangan</legend>
		<?php if($approval->keterangan != ''){ ?>
			Keterangan : <?php echo $approval->keterangan; ?>
		<?php }else{ ?>	
			Keterangan : Tidak ada keterangan.
		<?php } ?>
	</div>
</div>

<script>
	$(function () {
		$('#listbarang').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
	})
</script>
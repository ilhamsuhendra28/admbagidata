<style type="text/css">
/* Style For Chart Theme Colors */
.highcharts-title {
    color: #4D4F5C;
    font-size: 24px;
    fill: #4D4F5C;
    font-family: 'Barlow', sans-serif;
    font-weight: 700;
}


text,
.highcharts-color-0 .highcharts-point,
.highcharts-color-1 .highcharts-point,
.highcharts-color-2 .highcharts-point,
.highcharts-color-3 .highcharts-point {stroke: none;}
.highcharts-spline-series .highcharts-point,
.highcharts-spline-series .highcharts-point,
.highcharts-spline-series .highcharts-point,
.highcharts-spline-series .highcharts-point {fill: none !important;}

.highcharts-color-0 {
    fill: #EC008C;
    stroke: #EC008C;
    rx: 2;
    ry: 2;
}
.highcharts-color-0 text, .highcharts-color-0 text,
.highcharts-color-0 .highcharts-point {fill: #EC008C;}
.highcharts-color-0 .highcharts-graph {stroke: #EC008C;}


.highcharts-color-1 {
    fill: #222E61;
    stroke: #222E61;
    rx: 2;
    ry: 2;
}
.highcharts-color-1 text, .highcharts-color-1 text,
.highcharts-color-1 .highcharts-point {fill: #222E61;}
.highcharts-color-1 > .highcharts-graph {stroke: #222E61;}


.highcharts-color-2 {
    fill: #939FD0;
    stroke: #939FD0;
    rx: 2;
    ry: 2;
}
.highcharts-color-2 text, .highcharts-color-2 text,
.highcharts-color-2 .highcharts-point {fill: #939FD0;}
.highcharts-color-2 > .highcharts-graph {stroke: #939FD0;}


.highcharts-color-3 {
    fill: #FFB60A;
    stroke: #FFB60A;
    rx: 2;
    ry: 2;
}
.highcharts-color-3 text, .highcharts-color-3 text,
.highcharts-color-3 .highcharts-point {fill: #FFB60A;}
.highcharts-color-3 > .highcharts-graph {stroke: #FFB60A;}


.highcharts-color-4 {
    fill: #5EE2A0;
    stroke: #5EE2A0;
    rx: 2;
    ry: 2;
}
.highcharts-color-4 text, .highcharts-color-4 text,
.highcharts-color-4 .highcharts-point {fill: #5EE2A0;}
.highcharts-color-4 > .highcharts-graph {stroke: #5EE2A0;}

/* END */
</style>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-6 col-xs-6">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?php echo count($user); ?></h3>
					<p>Jumlah Users Aktif</p>
				</div>
				<div class="icon">
					<i class="ion ion-person"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>1)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-6 col-xs-6">
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3><?php echo count($receipt); ?></h3>

					<p>Jumlah User Yang Sudah Upload Receipt</p>
				</div>
				<div class="icon">
					<i class="ion ion-folder"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>3)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-6">
			<div id="user-chart"></div>
		</div>
		<div class="col-xs-6">
			<div id="upload-chart"></div>
		</div>
	</div>

	<div class="row">
		<?php foreach ($listSosmed as $key => $value) {
			$c = new CDbCriteria();
			$c->condition = 'datetime IS NOT NULL AND id_social_media = :sosmed_id';
			$c->params = array(':sosmed_id'=>$value->id);
			$sosmed = AccessToken::model()->findAll($c);

      $q = new CDbCriteria();
      $q->select = "COUNT(*) AS jml, CAST(datetime AS DATE) AS tgl";
      $q->condition = "id_social_media = :param AND datetime IS NOT NULL";
      $q->params = array(":param"=>$value->id);
      $q->group = "CAST(datetime AS DATE)";
      $connect = AccessToken::model()->findAll($q);

      $arr_tgl = [];
      $arr_jml_tgl = [];
      foreach ($connect as $vals) {
        $arr_tgl[] = $vals['tgl'];
        $arr_jml_tgl[$vals['tgl']] = $vals['jml'];
      }
      $awal = min($arr_tgl);
      $akhir = max($arr_tgl);

      $awal = new DateTime($awal);
      $akhir = new DateTime($akhir);
      $akhir = $akhir->modify( '+1 day' ); 

      $rangetgl = new DatePeriod($awal, new DateInterval('P1D'), $akhir);

      $jml_user = [];
      $arrtgl = [];
      $i=0;
      foreach($rangetgl as $date){
        if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl)) {
          $jml_user[$date->format("Y-m-d")] = $arr_jml_tgl[$date->format("Y-m-d")];
        } else {
          $jml_user[$date->format("Y-m-d")] = 0;
        }
        $arrtgl[$i] = $date->format("Y-m-d");
        $i++;
      }

		?>
		<div class="col-lg-4 col-xs-4">
			<div class="small-box bg-aqua">
				<div class="inner">
					<h3><?php echo count($sosmed); ?></h3>

					<p>Jumlah User yang Connect ke <?php echo $value->social_media; ?></p>
				</div>
				<div class="icon">
					<i class="ion ion-social-<?php echo strtolower($value->social_media); ?>"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>'sos'.$value->id)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
      <div id="connect-<?php echo $value->id ?>"></div>
		</div>

    <script type="text/javascript">
      Highcharts.chart('connect-<?php echo $value->id ?>', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [
            <?php 
              foreach ($arrtgl as $key => $tgl) {
                echo "'".$tgl."',";
              }
            ?>
            ],
            crosshair: true
        }],
        yAxis: [{ // First yAxis
            labels: {
                format: '{value}',
                style: {
                    color: 'rgba(67,66,93,.5)'
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            }
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            x: 30,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'User Connect',
            type: 'spline',
            data: [
            <?php
        foreach ($jml_user as $key => $val) {
            echo $val.",";
        }
        ?>
        ]
        }]
    });
    </script>
		<?php
		} ?>
	</div>

	<div class="row">
		<div class="col-lg-4 col-xs-4">
			<div class="small-box bg-red">
				<div class="inner">
					<h3><?php echo count($retail); ?></h3>
					<p>Jumlah Receipt Retail Yang Diupload</p>
				</div>
				<div class="icon">
					<i class="ion ion-ios-paper"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>4, 'id_upload_type'=>1)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-4 col-xs-4">
			<div class="small-box bg-green">
				<div class="inner">
					<h3><?php echo count($flight); ?></h3>

					<p>Jumlah Receipt Flight Yang Diupload</p>
				</div>
				<div class="icon">
					<i class="ion ion-plane"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>5, 'id_upload_type'=>3)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>

		<div class="col-lg-4 col-xs-4">
			<div class="small-box bg-blue">
				<div class="inner">
					<h3><?php echo count($train); ?></h3>

					<p>Jumlah Receipt Train Yang Diupload</p>
				</div>
				<div class="icon">
					<i class="ion ion-android-train"></i>
				</div>
				<a href="<?php echo Yii::app()->createUrl('admin/indexdetail', array('id'=>6, 'id_upload_type'=>2)); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-4">
			<div id="retail-chart"></div>
		</div>
		<div class="col-xs-4">
			<div id="kai-chart"></div>
		</div>
		<div class="col-xs-4">
			<div id="pesawat-chart"></div>
		</div>
	</div>
</section>

<script type="text/javascript">
	Highcharts.chart('user-chart', {
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: [{
          categories: [
          <?php 
          	foreach ($arrdate as $key => $value) {
          		echo "'".$value."',";
          	}
          ?>
          ],
          crosshair: true
      }],
      yAxis: [{ // First yAxis
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[0]
              }
          }
      }, { // Second yAxis
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[1]
              }
          },
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          opposite: true
      }],
      tooltip: {
          shared: true
      },
      legend: {
          layout: 'horizontal',
          align: 'center',
          x: 30,
          verticalAlign: 'top',
          y: 0,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
          name: 'User Active',
          type: 'spline',
          data: [
          <?php
			foreach ($user_active as $key => $active) {
			    echo $active.",";
			}
			?>
			]
      }, {
          name: 'User Register',
          type: 'spline',
          data: [
          <?php
            foreach ($user_register as $key => $reg) {
                echo $reg.",";
            }
        	?>
        	]
      }]
  });


	Highcharts.chart('upload-chart', {
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: [{
          categories: [
          <?php 
          	foreach ($arrdate as $key => $value) {
          		echo "'".$value."',";
          	}
          ?>
          ],
          crosshair: true
      }],
      yAxis: [{ // First yAxis
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[0]
              }
          }
      }],
      tooltip: {
          shared: true
      },
      legend: {
          layout: 'horizontal',
          align: 'center',
          x: 30,
          verticalAlign: 'top',
          y: 0,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
          name: 'User Upload',
          type: 'spline',
          data: [
          <?php
			foreach ($user_upload_all as $key => $upload) {
			    echo $upload.",";
			}
			?>
			]
      }]
  });


	Highcharts.chart('retail-chart', {
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: [{
          categories: [
          <?php 
          	foreach ($arrdate as $key => $value) {
          		echo "'".$value."',";
          	}
          ?>
          ],
          crosshair: true
      }],
      yAxis: [{ // First yAxis
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[0]
              }
          }
      }],
      tooltip: {
          shared: true
      },
      legend: {
          layout: 'horizontal',
          align: 'center',
          x: 30,
          verticalAlign: 'top',
          y: 0,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
          name: 'User Upload',
          type: 'spline',
          data: [
          <?php
			foreach ($user_retail as $key => $upload) {
			    echo $upload.",";
			}
			?>
			]
      }]
  });


	Highcharts.chart('kai-chart', {
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: [{
          categories: [
          <?php 
          	foreach ($arrdate as $key => $value) {
          		echo "'".$value."',";
          	}
          ?>
          ],
          crosshair: true
      }],
      yAxis: [{ // First yAxis
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[0]
              }
          }
      }],
      tooltip: {
          shared: true
      },
      legend: {
          layout: 'horizontal',
          align: 'center',
          x: 30,
          verticalAlign: 'top',
          y: 0,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
          name: 'User Upload',
          type: 'spline',
          data: [
          <?php
			foreach ($user_kai as $key => $upload) {
			    echo $upload.",";
			}
			?>
			]
      }]
  });


	Highcharts.chart('pesawat-chart', {
      chart: {
          zoomType: 'xy'
      },
      title: {
          text: ''
      },
      subtitle: {
          text: ''
      },
      xAxis: [{
          categories: [
          <?php 
          	foreach ($arrdate as $key => $value) {
          		echo "'".$value."',";
          	}
          ?>
          ],
          crosshair: true
      }],
      yAxis: [{ // First yAxis
          labels: {
              format: '{value}',
              style: {
                  color: 'rgba(67,66,93,.5)'
              }
          },
          title: {
              text: '',
              style: {
                  color: Highcharts.getOptions().colors[0]
              }
          }
      }],
      tooltip: {
          shared: true
      },
      legend: {
          layout: 'horizontal',
          align: 'center',
          x: 30,
          verticalAlign: 'top',
          y: 0,
          floating: true,
          backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
      },
      series: [{
          name: 'User Upload',
          type: 'spline',
          data: [
          <?php
			foreach ($user_pesawat as $key => $upload) {
			    echo $upload.",";
			}
			?>
			]
      }]
  });
</script>
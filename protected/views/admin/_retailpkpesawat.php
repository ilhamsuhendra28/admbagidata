<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pk-pesawat-form',
	'enableAjaxValidation'=>false
)); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="error" style="display:none"></div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Identitas Penumpang</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'no_identitas'); ?>
				<?php echo $form->textField($receiptpkpesawat,'no_identitas',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'no_identitas'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Penerbangan</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'nama_maskapai'); ?>
				<?php echo $form->textField($receiptpkpesawat,'nama_maskapai',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'nama_maskapai'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'no_penerbangan'); ?>
				<?php echo $form->textField($receiptpkpesawat,'no_penerbangan',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'no_penerbangan'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'kelas_pesawat'); ?>
				<?php echo $form->textField($receiptpkpesawat,'kelas_pesawat',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'kelas_pesawat'); ?>
			</div>
		</div>
		<div class="col-md-1">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'gate'); ?>
				<?php echo $form->textField($receiptpkpesawat,'gate',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'gate'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'no_kursi'); ?>
				<?php echo $form->textField($receiptpkpesawat,'no_kursi',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'no_kursi'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'berangkat_dari'); ?>
				<?php echo $form->textField($receiptpkpesawat,'berangkat_dari',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'berangkat_dari'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'tujuan_ke'); ?>
				<?php echo $form->textField($receiptpkpesawat,'tujuan_ke',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'tujuan_ke'); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Jadwal</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkpesawat,'tgl_berangkat'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_berangkat',  
						'language' => 'en',  
						'model' => $receiptpkpesawat,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkpesawat,'tgl_berangkat'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'jam_berangkat'); ?>
				<?php echo $form->textField($receiptpkpesawat,'jam_berangkat',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'jam_berangkat'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkpesawat,'tgl_sampai'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_sampai',  
						'language' => 'en',  
						'model' => $receiptpkpesawat,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkpesawat,'tgl_sampai'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'jam_sampai'); ?>
				<?php echo $form->textField($receiptpkpesawat,'jam_sampai',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'jam_sampai'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Tiket</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'nomor_tiket'); ?>
				<?php echo $form->textField($receiptpkpesawat,'nomor_tiket',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'nomor_tiket'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'harga_tiket'); ?>
				<?php echo $form->textField($receiptpkpesawat,'harga_tiket',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'harga_tiket'); ?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'jenis_pembayaran'); ?>
				<?php echo $form->textField($receiptpkpesawat,'jenis_pembayaran',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'jenis_pembayaran'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($receiptpkpesawat,'tgl_pembelian_tiket'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'attribute' =>'tgl_pembelian_tiket',  
						'language' => 'en',  
						'model' => $receiptpkpesawat,  
						'options' => array(  
							'name'=>'datepicker-month-year-menu',
							'flat'=>true,//remove to hide the datepicker
							'changeMonth'=>true,
							'changeYear'=>true,
							'showButtonPanel'=>true,
							'dateFormat' => 'yy-mm-dd',  
							'showAnim' => 'fold'
						),  
						'htmlOptions' => array(
							'style' => 'width:100%;',
							'class'=>'form-control' 
							
						)  
						)  
					);  
				?>
				<?php echo $form->error($receiptpkpesawat,'tgl_pembelian_tiket'); ?>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">    
				<?php echo $form->labelEx($receiptpkpesawat,'jam_pembelian_tiket'); ?>
				<?php echo $form->textField($receiptpkpesawat,'jam_pembelian_tiket',array('size'=>60,'maxlength'=>20, 'class'=>'form-control')); ?>
				<?php echo $form->error($receiptpkpesawat,'jam_pembelian_tiket'); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">	
			<legend>Informasi Hadiah</legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<?php echo CHtml::dropDownList('hadiah', '', array('1' => 'Point', '2' => 'Cash'),array('prompt'=>'Pilih Hadiah', ' class'=>'form-control', 'onchange'=>'pilihhadiahPk($(this).val())')); ?>
		</div>
		<div class="col-md-3 tampilhadiahpk" style="display:none;">
			<?php echo CHtml::textField('hadiahvalue', '',array('placeholder'=>'Cash atau Point', 'class'=>'form-control')); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">  
				<?php echo CHtml::textField('keterangan', '',array('placeholder'=>'Keterangan', 'class'=>'form-control')); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<button type="submit" class="btn btn-success" onclick="savePesawat(2)"><i class="fa fa-check"></i> Approve</button>
			<button type="submit" class="btn btn-danger" onclick="savePesawat(3)"><i class="fa fa-remove"></i> Reject</button>
		</div>
	</div>
<?php $this->endWidget(); ?>

<script>
function savePesawat(id_approval_pesawat){
	event.preventDefault();
	var data = new FormData($("#pk-pesawat-form")[0]);
	jQuery.ajax({
		type: 'POST',
		dataType: "json",
		processData: false,
		contentType: false,
		url: '<?php echo Yii::app()->createAbsoluteUrl('admin/saveReceiptPesawat'); ?>id/'+<?php echo $receiptpk2->id_pk; ?>+'/approval/'+id_approval_pesawat,
		data: data,
		
		beforeSend: function() {
			swal({
				title : "<div style='background:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif) no-repeat top center;height: 50px;overflow: hidden;'></div> Loading...",
				showCancelButton: false,
				showConfirmButton: false,
				allowOutsideClick: false,
				width: 400,
				padding: 100
			}).catch(swal.noop);
		},
		
		success:function(data){
			if(data.hasil == 'success'){
				window.location = "<?php echo Yii::app()->createAbsoluteUrl('admin/receipt', array('id'=>$id_approval)); ?>";
			}else{
				swal.close();
				$('.error').html(data.hasil);
				$('.error').show();
			}	
		},
	});
}	
</script>
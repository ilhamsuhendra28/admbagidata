<?php
	if($id == 1){
		$tampil = 'In Progress';
	}else if($id == 2){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>

<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Receipt - <?php echo $tampil; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Control Panel List Receipt - <?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<table id="listreceipt" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Reeipt</th>
								<th>Keterangan</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if(!empty($model)){
									foreach($model as $mrow){							
							?>
									<tr>
										
										<td><a href="<?php echo Yii::app()->createUrl('admin/tampilreceipt', array('id'=>$mrow->id, 'id_approval'=>$id)); ?>"><?php echo $mrow->image_name; ?></a></td>
										<td><?php echo $mrow->keterangan; ?></td>
										<?php if($id == 1){ ?>
											<td>
												<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('admin/receiptdetail', array('id'=>$mrow->id, 'kategori'=>'validasi', 'id_approval'=>$id)) ?>">Validasi</a>
												<a class="btn btn-danger" href="<?php echo Yii::app()->createUrl('admin/receiptdetail', array('id'=>$mrow->id, 'kategori'=>'rejected', 'id_approval'=>$id)) ?>">Reject</a>
											</td>
										<?php }else{ ?>
											<td>
												<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('admin/viewreceipt', array('id'=>$mrow->id, 'id_approval'=>$id)) ?>">View</a>
												<a class="btn btn-warning" href="<?php echo Yii::app()->createUrl('admin/editReceipt', array('id'=>$mrow->id, 'id_approval'=>$id)) ?>">Edit</a>
											</td>
										<?php } ?>
									</tr>
								<?php 
									} 
								}else{
								?>
									<tr><td colspan="3" style="text-align:center;">No matching records found</td></tr> 
								<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	$(function () {
		$('#listreceipt').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
	})
</script>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Referral</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Control Panel List Referral></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<table id="viewreferral" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Referral</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if(!empty($all_kode)){
									foreach($all_kode as $mrow){	
										$jml_user = count(User::model()->findAllByAttributes(array('active'=>1,'ref_code'=>$mrow->kode)));						
							?>
									<tr>
										
										<td><?php echo $mrow->kode; ?></a></td>
										<td><?php echo $jml_user; ?></td>
									</tr>
							<?php 
									} 
								}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	$(function () {
		$('#viewreferral').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
	})
</script>
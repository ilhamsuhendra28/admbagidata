<div class="row">
	<div class="col-md-12">
		<center>
			<?php if($exp[0] == 'image'){ ?>
				<img height="350" src="<?php echo 'https://app.bagidata.com'.$upload->image_path; ?>"/>
			<?php }else{ ?>
				<?php echo CHtml::link($upload->image_name, array('admin/viewpdf', 'id'=>$id), array('target'=>'_blank')); ?>
			<?php } ?>
		</center>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="error" style="display:none"></div>
		<div class="errorSummary" style="display:none"></div>
	</div>
</div>

<?php 
	$receipt2 = ReceiptRetail:: model()->findByAttributes(array('id_user_upload_data'=>$id));
	if(empty($receipt2)){
?>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'retail-form',
		'enableAjaxValidation'=>false
	)); ?>	
	
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">    
						<?php echo $form->labelEx($receipt,'nama_toko'); ?>
						<?php echo $form->textField($receipt,'nama_toko',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<?php echo $form->error($receipt,'nama_toko'); ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<?php echo $form->labelEx($receipt,'tgl_pembelian'); ?>
						<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
								'attribute' =>'tgl_pembelian',  
								'language' => 'en',  
								'model' => $receipt,  
								'options' => array(  
									'name'=>'datepicker-month-year-menu',
									'flat'=>true,//remove to hide the datepicker
									'changeMonth'=>true,
									'changeYear'=>true,
									'showButtonPanel'=>true,
									'dateFormat' => 'yy-mm-dd',  
									'showAnim' => 'fold'
								),  
								'htmlOptions' => array(
									'style' => 'width:100%;',
									'class'=>'form-control'
								)  
								)  
							);  
						?>
						<?php echo $form->error($receipt,'tgl_pembelian'); ?>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">    
						<?php echo $form->labelEx($receipt,'jam_pembelian'); ?>
						<?php echo $form->textField($receipt,'jam_pembelian',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
						<?php echo $form->error($receipt,'jam_pembelian'); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">    
						<?php echo $form->labelEx($receipt,'alamat_toko'); ?>
						<?php echo $form->textArea($receipt,'alamat_toko',array('rows'=>6, 'class'=>'form-control')); ?>
						<?php echo $form->error($receipt,'alamat_toko'); ?>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<?php echo CHtml::submitButton('Validasi', array('class'=>'btn btn-success validasi', 'onclick'=>'addRetail()')); ?>
			</div>
	<?php $this->endWidget(); ?>
<?php }else{ ?>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'retail-form',
		'enableAjaxValidation'=>false
	)); ?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">    
					<?php echo $form->labelEx($receipt2,'nama_toko'); ?>
					<?php echo $form->textField($receipt2,'nama_toko',array('size'=>60,'maxlength'=>255, 'class'=>'form-control', 'disabled'=>true)); ?>
					<?php echo $form->error($receipt2,'nama_toko'); ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?php echo $form->labelEx($receipt2,'tgl_pembelian'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							'attribute' =>'tgl_pembelian',  
							'language' => 'en',  
							'model' => $receipt2,  
							'options' => array(  
								'name'=>'datepicker-month-year-menu',
								'flat'=>true,//remove to hide the datepicker
								'changeMonth'=>true,
								'changeYear'=>true,
								'showButtonPanel'=>true,
								'dateFormat' => 'yy-mm-dd',  
								'showAnim' => 'fold'
							),  
							'htmlOptions' => array(
								'style' => 'width:100%;',
								'class'=>'form-control',
								'disabled'=>true
							)  
							)  
						);  
					?>
					<?php echo $form->error($receipt2,'tgl_pembelian'); ?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">    
					<?php echo $form->labelEx($receipt2,'jam_pembelian'); ?>
					<?php echo $form->textField($receipt2,'jam_pembelian',array('size'=>60,'maxlength'=>255, 'class'=>'form-control', 'disabled'=>true)); ?>
					<?php echo $form->error($receipt2,'jam_pembelian'); ?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">    
						<?php echo $form->labelEx($receipt2,'alamat_toko'); ?>
						<?php echo $form->textArea($receipt2,'alamat_toko',array('rows'=>6, 'class'=>'form-control', 'readonly'=>true)); ?>
						<?php echo $form->error($receipt2,'alamat_toko'); ?>
					</div>
				</div>
			</div>
		</div>
	<?php $this->endWidget(); ?>
	
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'barang-form',
		'enableAjaxValidation'=>false
	)); ?>
		<div class="row">
			<div class="col-md-12">
				<button type="button" class="btn btn-success" onclick="addBarang()"><i class="fa fa-plus"></i> Tambah Barang</button>
			</div>
		</div>
		<div id="appendbarang"></div>
		
		<div class="row tampilall" style="display:none;">
			<div class="col-md-3">
				<?php echo CHtml::dropDownList('hadiah', '', array('1' => 'Point', '2' => 'Cash'),array('prompt'=>'Pilih Hadiah', ' class'=>'form-control', 'onchange'=>'pilihhadiah($(this).val())')); ?>
			</div>
			<div class="col-md-3 tampilhadiah" style="display:none;">
				<?php echo CHtml::textField('hadiahvalue', '',array('placeholder'=>'Cash atau Point', 'class'=>'form-control')); ?>
			</div>
		</div>
		
		<div class="row">
		<div class="col-md-12">
			<div class="form-group">  
				<?php echo CHtml::textField('keterangan', '',array('placeholder'=>'Keterangan', 'class'=>'form-control')); ?>
			</div>
		</div>
	</div>
		
		<div class="row">
			<div class="col-md-12">
				<button type="submit" style="display:none;" class="btn btn-success tampilall" onclick="saveBarang(2)"><i class="fa fa-check"></i> Approve</button>
				<button type="submit" style="display:none;" class="btn btn-danger tampilall" onclick="saveBarang(3)"><i class="fa fa-remove"></i> Reject</button>
			</div>
		</div>
		
		<div id="barangCounter" hidden>0</div>
		<?php echo CHtml::hiddenField('idbarang[0]', '', array('id'=>'idbarang[0]')); ?>
	<?php $this->endWidget(); ?>
	
	<script>
	function saveBarang(obj){
		event.preventDefault();
		var data = new FormData($("#barang-form")[0]);
		jQuery.ajax({
			type: 'POST',
			dataType: "json",
			processData: false,
			contentType: false,
			url: '<?php echo Yii::app()->createAbsoluteUrl('admin/saveBarang'); ?>id/'+<?php echo $receipt2->id_retail; ?>+'/approval/'+obj,
			data: data,
			
			beforeSend: function() {
				swal({
					title : "<div style='background:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif) no-repeat top center;height: 50px;overflow: hidden;'></div> Loading...",
					showCancelButton: false,
					showConfirmButton: false,
					allowOutsideClick: false,
					width: 400,
					padding: 100
				}).catch(swal.noop);
			},
			
			success:function(data){
				if(data.hasil == 'success'){
					window.location = "<?php echo Yii::app()->createAbsoluteUrl('admin/receipt', array('id'=>$id_approval)); ?>";
				}
			},
		});
	}	
	</script>
	
<?php } ?>

<div class="notvalid" style="display:none">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'rejected-form',
		'enableAjaxValidation'=>false
	)); ?>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">  
					<?php echo $form->labelEx($keterangan,'keterangan'); ?>
					<?php echo $form->textArea($keterangan,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
					<?php echo $form->error($keterangan,'keterangan'); ?>
				</div>
			</div>
		</div>
		
		<div class="box-footer">
			<?php echo CHtml::submitButton('Reject', array('class'=>'btn btn-danger')); ?>
		</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $kategoribarang = KategoriBarang::model()->findAll();  ?>
<?php $satuanbarang = SatuanBarang::model()->findAll();  ?>

<script>
function addRetail(){
	event.preventDefault();
	var data = new FormData($("#retail-form")[0]);
	jQuery.ajax({
		type: 'POST',
		dataType: "json",
		processData: false,
		contentType: false,
		url: '<?php echo Yii::app()->createAbsoluteUrl('admin/saveRetail', array('id'=>$id)); ?>',
		data: data,
		
		beforeSend: function() {
			swal({
				title : "<div style='background:url(<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif) no-repeat top center;height: 50px;overflow: hidden;'></div> Loading...",
				showCancelButton: false,
				showConfirmButton: false,
				allowOutsideClick: false,
				width: 400,
				padding: 100
			  }).catch(swal.noop);
		},
		success:function(data){
			if (data.hasil == 'success') {
				window.location = "<?php echo Yii::app()->createAbsoluteUrl('admin/receiptdetail',array('id'=>$id, 'kategori'=>$kategori, 'id_approval'=>$id_approval)); ?>";
			}else{
				if(data.hasil == 'notvalid'){
					swal.close();
					$('.error').hide();
					$('.errorSummary').html(data.notvalid);
					$('.errorSummary').show();
					$('.notvalid').show();
					$('.validasi').hide();
					$("#retail-form input").each(function(){
						$(this).attr("disabled", true);
					});
				}else{	
					swal.close();
					$('.error').html(data.hasil);
					$('.error').show();
				}
			}	
		}
	});
}
	
function pilihhadiah(obj){
	if(obj != ''){
		$('.tampilhadiah').show();
	}else{
		$('.tampilhadiah').hide();
	}
}	
function delBarang(valueT){
	$("#"+valueT).remove();
	var valueT = document.getElementById("barangCounter").innerHTML;
	valueT = Number(valueT) - 1;
	document.getElementById("barangCounter").innerHTML = valueT;
	if(valueT == 0){
		$('.tampilall').hide();
	}	
}
function addBarang(){
	$('.tampilall').show();
	var valueT = document.getElementById("barangCounter").innerHTML;
	valueT = Number(valueT) + 1;
	document.getElementById("barangCounter").innerHTML = valueT;

	$("#appendbarang").append("<div id=appendbarang"+valueT+" class='form-search'><div class='row'><div class='col-md-3'><input type='hidden' id=idbarang["+valueT+"] name=idbarang["+valueT+"]><input placeholder='Nama Barang' class='form-control' type='text' id=nama_barang["+valueT+"] name=nama_barang["+valueT+"]></div><div class='col-md-1'><input placeholder='Jumlah Barang' class='form-control angkabarangharga' type='text' id=jumlah_barang["+valueT+"] name='jumlah_barang["+valueT+"]'></div><div class='col-md-2'><select id=satuan_barang["+valueT+"] name=satuan_barang["+valueT+"]  class='form-control'><option>Pilih Satuan Barang</option><?php if(!empty($satuanbarang)){foreach($satuanbarang as $sbarang){echo "<option value='".$sbarang->id_satuan_barang."'>".$sbarang->satuan_barang."</option>";}}?></select></div><div class='col-md-2'><input placeholder='Harga Barang' type='text' class='form-control angkabarangharga' id=harga_barang["+valueT+"] name=harga_barang["+valueT+"]></div><div class='col-md-3'><select id=kategori_barang["+valueT+"] name=kategori_barang["+valueT+"]  class='form-control'><option>Pilih Kategori Barang</option><?php if(!empty($kategoribarang)){foreach($kategoribarang as $kbarang){echo "<option value='".$kbarang->id_kategori_barang."'>".$kbarang->kategori_barang."</option>";}}?></select></div><div class='col-md-1'><button class='btn btn-sm btn-info' style='height:30px;margin-top:2px;' type=button name=delete title='Delete' onClick=delBarang(id='appendbarang"+valueT+"')><i class='fa fa-remove'></i></button></div></div></div>");
	
	$(".angkabarangharga").each(function(){
		$(this).keypress(function (data){ 
			if(data.which!=8 && data.which!=0 && data.which!=46 && (data.which<48 || data.which>57)){
				return false;
		   	}    
		});
	});
}

$(window).load(function() {
	$("#hadiahvalue").each(function(){
		$(this).keypress(function (data){ 
			if(data.which!=8 && data.which!=0 && (data.which<48 || data.which>57)){
				return false;
		   	}    
		});
	});
	
	
});
</script>
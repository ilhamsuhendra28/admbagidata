<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<div class="row">
	<div class="col-lg-12">
		<div class="form">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

				<p class="login-box-msg">Sign in to start your session</p>
				
				<div class="form-group has-feedback">
					<div class="row">
						<?php echo $form->labelEx($model,'username'); ?>
						<?php echo $form->textField($model,'username', array('class'=>'form-control')); ?>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						<?php echo $form->error($model,'username'); ?>
					</div>
				</div>
				<div class="form-group has-feedback">
					<div class="row">
						<?php echo $form->labelEx($model,'password'); ?>
						<?php echo $form->passwordField($model,'password', array('class'=>'form-control')); ?>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						<?php echo $form->error($model,'password'); ?>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-8">
						<div class="checkbox icheck">
							<label>
								<?php echo $form->checkBox($model,'rememberMe'); ?>
								<?php echo $form->label($model,'rememberMe'); ?>
								<?php echo $form->error($model,'rememberMe'); ?>
							</label>
						</div>
					</div>
					
					<div class="col-xs-4">
						<?php echo CHtml::submitButton('Login', array('class'=>'btn btn-primary btn-block btn-flat')); ?>
					</div>
				</div>

			<?php $this->endWidget(); ?>
		</div><!-- form -->
	</div>
</div>
<?php
	if($promo->approval == 0){
		$tampil = 'In Progress';
	}else if($promo->approval == 1){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Receipt - <?php echo $tampil.' - '.$kategori; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="<?php echo Yii::app()->createUrl('promo/index', array('id'=>$promo->approval)); ?>">Control Panel List Receipt - <?php echo $tampil; ?></a></li>
		<li class="active"><?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Campaign Detail</h3>
                </div>
                
                <div class="box-body form-horizontal">
                    <div class="form-group">
                    <?php
                    foreach($promo->image as $key => $value){
                        echo '<div class="col-sm-6 text-center">'.CHtml::image(MyCollection::getImageUrl($value->image, 'user'),'',array('height'=>'200px')).'</div>';
                    }
                    ?>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-sm-2 control-label">Company</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $promo->id_company;?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Kategori</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $promo->category->category;?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $promo->title;?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Content</label>

                        <div class="col-sm-10">
                            <textarea class="form-control" readonly="" rows="5"> <?php echo $promo->content;?> </textarea>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Point</label>

                        <div class="col-sm-2">
                            <div style="margin-bottom:10px;" class="form-control"> <?php echo $promo->point;?> </div>
                        </div>
                        
                        <label for="subject" class="col-sm-2 control-label">Cash</label>

                        <div class="col-sm-2">
                            <div style="margin-bottom:10px;" class="form-control"> <?php echo $promo->cash;?> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Max Buys</label>

                        <div class="col-sm-2">
                            <p class="form-control"><?php echo $promo->max_buy;?></p>
                        </div>

                        <label for="post-date" class="col-sm-2 control-label">Begin Data</label>

                        <div class="col-sm-2">
                            <p class="form-control"><?php echo $promo->begda;?></p>
                        </div>

                        <label for="post-date" class="col-sm-2 control-label">End Date</label>

                        <div class="col-sm-2">
                            <p class="form-control"><?php echo $promo->endda;?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Jenis</label>

                        <div class="col-sm-2">
                            <h4> 
                            <?php
                            if ($promo->approval == 0)
                                echo '<span class="label label-warning">Need Approval</span>';
                            else if ($promo->approval == 1)
                                echo '<span class="label label-success">Approved</span>';
                            else if ($promo->approval == 2)
                                echo '<span class="label label-danger">Rejected</span>';
                            ?>
                            </h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pesan" class="col-sm-2 control-label">Pesan</label>
                        <div class="col-sm-10">
                            <p class="form-control"><?php echo $promo->approval_message;?></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-sm-8">
                        <a href="<?php echo Yii::app()->createUrl('promo/index',array('id'=>$promo->approval));?>" class="btn btn-default">Back</a>
                    </div>
                </div>
                <!-- /.box-footer -->
			<div>
		</div>
	</div>
</section>
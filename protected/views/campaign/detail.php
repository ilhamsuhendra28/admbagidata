<?php
	if($campaign->approval == 0){
		$tampil = 'In Progress';
	}else if($campaign->approval == 1){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>
<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Campaign - <?php echo $tampil.' - '.$kategori; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li><a href="<?php echo Yii::app()->createUrl('campaign/index', array('id'=>$campaign->approval)); ?>">Control Panel List Campaign - <?php echo $tampil; ?></a></li>
		<li class="active"><?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Campaign Detail</h3>
                </div>
                
                <div class="box-body form-horizontal">
                    <div class="form-group">
                    <?php
                    foreach($campaign->image as $key => $value){
                        echo '<div class="col-sm-6 text-center">'.CHtml::image(MyCollection::getImageUrl($value->image, 'enterprise'),'',array('height'=>'200px')).'</div>';
                    }
                    ?>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-sm-2 control-label">User</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $campaign->user->full_name;?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">Title</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $campaign->title;?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Subject</label>

                        <div class="col-sm-10">
                            <p class="form-control"> <?php echo $campaign->subject;?> </p>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Description</label>

                        <div class="col-sm-10">
                            <div style="margin-bottom:10px;" class="form-control"> <?php echo $campaign->description;?> </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Jenis</label>

                        <div class="col-sm-2">
                            <p class="form-control"> 
                            <?php
                            if ($campaign->email_blast)
                                echo 'Email Blast';
                            else if ($campaign->sms_blast)
                                echo 'SMS Blast';
                            else if ($campaign->wa_blast)
                                echo 'WhatsApp Blast';
                            ?>
                            </p>
                        </div>

                        <label for="post-date" class="col-sm-2 control-label">Post Date</label>

                        <div class="col-sm-2">
                            <p class="form-control"><?php echo $campaign->post_date;?></p>
                        </div>

                        <label for="post-date" class="col-sm-2 control-label">Post Time</label>

                        <div class="col-sm-2">
                            <p class="form-control"><?php echo $campaign->post_time;?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subject" class="col-sm-2 control-label">Status</label>

                        <div class="col-sm-2">
                            <h4>
                            <?php
                            if ($campaign->approval == 0)
                                echo '<span class="label label-warning">Need Approval</span>';
                            else if ($campaign->approval == 1)
                                echo '<span class="label label-success">Approved</span>';
                            else if ($campaign->approval == 2)
                                echo '<span class="label label-danger">Rejected</span>';
                            ?>
                            </h4>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pesan" class="col-sm-2 control-label">Pesan</label>
                        <div class="col-sm-10">
                            <p class="form-control"><?php echo $campaign->approval_message;?></p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-sm-8">
                        <a href="<?php echo Yii::app()->createUrl('campaign/index',array('id'=>$campaign->approval));?>" class="btn btn-default">Back</a>
                    </div>
                </div>
                <!-- /.box-footer -->
			<div>
		</div>
	</div>
</section>
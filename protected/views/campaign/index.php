<?php
	if($id == 0){
		$tampil = 'In Progress';
	}else if($id == 1){
		$tampil = 'Approved';
	}else{
		$tampil = 'Rejected';
	}	
 ?>

<section class="content-header">
	<h1>
		Dashboard
		<small>Control Panel List Campaign - <?php echo $tampil; ?></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active">Control Panel List Campaign - <?php echo $tampil; ?></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<table id="listreceipt" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Title</th>
								<th>Keterangan</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								if(!empty($listCampaign)){
									foreach($listCampaign as $mrow){							
							?>
									<tr>
										
										<td><a href="<?php echo Yii::app()->createUrl('admin/tampilreceipt', array('id'=>$mrow->id, 'id_approval'=>$id)); ?>"><?php echo $mrow->subject; ?></a></td>
										<td><?php echo $mrow->title; ?></td>
										<?php if($id == 0){ ?>
											<td>
												<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('campaign/validasi', array('id'=>$mrow->id)) ?>">Validasi</a>
												
											</td>
										<?php }else{ ?>
											<td>
												<a class="btn btn-info" href="<?php echo Yii::app()->createUrl('campaign/validasi', array('id'=>$mrow->id)) ?>">View</a>
											</td>
										<?php } ?>
									</tr>
								<?php 
									} 
								}else{
								?>
									<tr><td colspan="3" style="text-align:center;">No matching records found</td></tr> 
								<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>


<script>
	$(function () {
		$('#listreceipt').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		});
	})
</script>
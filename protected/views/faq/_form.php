
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'faq-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>
	<div class="box-body">
		<div class="form-group">    
			<?php echo $form->labelEx($model,'question'); ?>
			<?php echo $form->textField($model,'question',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'question'); ?>
		</div>

		<div class="form-group">  
			<?php echo $form->labelEx($model,'answer'); ?>
			<?php echo $form->textArea($model,'answer',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'answer'); ?>
		</div>

		<div class="form-group">  
			<?php echo $form->labelEx($model,'icon'); ?>
			<?php echo $form->fileField($model,'icon',array('size'=>60,'maxlength'=>255, 'class'=>'form-control')); ?>
			<?php echo $form->error($model,'icon'); ?>
			<?php if($id != ''){ ?>
				<img src="<?php echo Yii::app()->theme->baseUrl.$model->icon; ?>" width="150"/>
			<?php } ?>
		</div>
	</div>
	<div class="box-footer">
		<?php echo CHtml::submitButton('Submit', array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

<script>
	CKEDITOR.replace('Faq_answer', {
            
			skin : 'office2013',
			filebrowserBrowseUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/browse.php?opener=ckeditor&type=files',
			filebrowserImageBrowseUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/browse.php?opener=ckeditor&type=images',
			filebrowserFlashBrowseUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/browse.php?opener=ckeditor&type=flash',
			filebrowserUploadUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/upload.php?opener=ckeditor&type=files',
			filebrowserImageUploadUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/upload.php?opener=ckeditor&type=images',
			filebrowserFlashUploadUrl : '<?php echo Yii::app()->theme->baseUrl; ?>/kcfinder/upload.php?opener=ckeditor&type=flash',
        });
</script>
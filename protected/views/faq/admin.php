<section class="content-header">
	<h1>
		Dashboard
		<small>CRUD FAQ</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('faq/admin'); ?>">Kelola FAQ</a></li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-body">
					<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('faq/create'); ?>">Tambah FAQ</a><br/><br/>
					<table id="crudfaq" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Question</th>
								<th>Answer</th>
								<th>Icon</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($model as $mrow){ 
							?>
								<tr>
									<td><?php echo $mrow->question; ?></td>
									<td><?php echo nl2br($mrow->answer); ?></td>
									<td><img src="<?php echo Yii::app()->theme->baseUrl.$mrow->icon; ?>" width="150"></td>
									<td style="text-align:center">
										<a href="<?php echo Yii::app()->createUrl('faq/update', array('id'=>$mrow->id)); ?>"><i class="fa fa-pencil"></i></a>
										<a href="<?php echo Yii::app()->createUrl('faq/deletedata', array('id'=>$mrow->id)); ?>"><i class="fa fa-remove"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$(function () {
		$('#crudfaq').DataTable({
			'paging'      : true,
			'lengthChange': true,
			'searching'   : true,
			'ordering'    : false,
			'info'        : true,
			'autoWidth'   : true
		})
	})
</script>
<section class="content-header">
	<h1>
		Dashboard
		<small>CRUD FAQ</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo Yii::app()->createUrl('admin'); ?>"><i class="fa fa-home"></i> Home</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('faq/admin'); ?>">Kelola FAQ</a></li>
		<li class="active"><a href="<?php echo Yii::app()->createUrl('faq/create'); ?>">Tambah FAQ</a></li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<?php $this->renderPartial('_form', array('model'=>$model)); ?>
			</div>
		</div>
	</div>
</div>
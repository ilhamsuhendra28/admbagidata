<?php
/* @var $this PromoCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Promo Categories',
);

$this->menu=array(
	array('label'=>'Create PromoCategory', 'url'=>array('create')),
	array('label'=>'Manage PromoCategory', 'url'=>array('admin')),
);
?>

<h1>Promo Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

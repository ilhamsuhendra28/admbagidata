<?php
/* @var $this PromoCategoryController */
/* @var $model PromoCategory */

$this->breadcrumbs=array(
	'Promo Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
//	array('label'=>'List PromoCategory', 'url'=>array('index')),
	array('label'=>'Create PromoCategory', 'url'=>array('create')),
	array('label'=>'Update PromoCategory', 'url'=>array('update', 'id'=>$model->id)),
	array(
            'label'=>'Delete PromoCategory',
            'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),
            'confirm'=>'Are you sure you want to delete this item?',
            'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)
        )),
	array('label'=>'Manage PromoCategory', 'url'=>array('admin')),
);
?>

<h1>View PromoCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'created_by',
		'created_date_time',
		'updated_by',
		'updated_date_time',
	),
)); ?>

<?php
/* @var $this PromoCategoryController */
/* @var $model PromoCategory */

$this->breadcrumbs=array(
	'Promo Categories'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List PromoCategory', 'url'=>array('index')),
	array('label'=>'Manage PromoCategory', 'url'=>array('admin')),
);
?>

<h1>Create PromoCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this PromoCategoryController */
/* @var $model PromoCategory */

$this->breadcrumbs=array(
	'Promo Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
//	array('label'=>'List PromoCategory', 'url'=>array('index')),
	array('label'=>'Create PromoCategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#promo-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Promo Categories</h1>
</div><!-- search-form -->
<div class="box">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'promo-category-grid',
    'htmlOptions'=>array('class'=>'box-body'),
    'itemsCssClass'=>'table table-bordered table-striped',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'category',
		'created_by',
//		'created_date_time',
//		'updated_by',
//		'updated_date_time',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>

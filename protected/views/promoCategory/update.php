<?php
/* @var $this PromoCategoryController */
/* @var $model PromoCategory */

$this->breadcrumbs=array(
	'Promo Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List PromoCategory', 'url'=>array('index')),
	array('label'=>'Create PromoCategory', 'url'=>array('create')),
	array('label'=>'View PromoCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PromoCategory', 'url'=>array('admin')),
);
?>

<h1>Update PromoCategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
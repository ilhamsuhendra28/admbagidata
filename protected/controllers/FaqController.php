<?php

class FaqController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'admin','deletedata','index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Faq('create');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Faq']))
		{
			$file = CUploadedFile::getInstance($model, 'icon');
			$model->attributes=$_POST['Faq'];
			
			$exp_ext = explode('.', $file->name);
			if(!empty($file)){    
				$pathcreate = Yii::getPathOfAlias('webroot.themes.admlte');
				$model->icon = '/images/icon/help/'.Logic::randomChar().'_'.date('Y-m-d').'.'.$exp_ext[count($exp_ext) - 1];
				$path=$pathcreate.$model->icon;  
				$file->saveAs($path);  
			}
			
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$icon = $model->icon;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Faq']))
		{
			$ada = $_POST['Faq']['icon'] = $model->icon;
			$model->attributes=$_POST['Faq'];
			
			$model->scenario='update';
			$file = CUploadedFile::getInstance($model, 'icon');
			$exp_ext = explode('.', $file->name);
			if(!empty($file)){
				if(!empty($ada)){
					unlink(Yii::getPathOfAlias('webroot.themes.admlte').$model->icon);
				}
				
				$pathcreate = Yii::getPathOfAlias('webroot.themes.admlte');
				$model->icon = '/images/icon/help/'.Logic::randomChar().'_'.date('Y-m-d').'.'.$exp_ext[count($exp_ext) - 1];
				$path=$pathcreate.$model->icon;  
				$file->saveAs($path);  
			}else{
				$model->icon = $icon;
			}
			
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
			'id'=>$id
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeleteData($id)
	{
		$model=Faq::model()->findByPk($id);
		$pathcreate = Yii::getPathOfAlias('webroot.themes.admlte');
		unlink($pathcreate.$model->icon);
		if($this->loadModel($id)->delete()){
			$this->redirect(array('admin'));
		}	
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Faq');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=Faq::model()->findAll(array('order'=>'id DESC'));
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Faq the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Faq::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Faq $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='faq-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

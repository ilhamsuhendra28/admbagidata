<?php

class AdminController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'indexdetail', 'receipt', 'tampilreceipt', 'receiptdetail', 'saveretail', 'savebarang', 'savereceiptkereta', 'savereceiptpesawat', 'viewreceipt', 'savepk', 'viewpdf','listFeedback','printFeedback'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('showReferral','showRef','showCupstuwerd'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actionIndex()
	{
		$user = User::model()->findAll(array('condition'=>'active = 1'));
		$listSosmed = SocialMedia::model()->active()->findAll();
		$receipt = UserUploadData::model()->findAll(array('condition'=>'id_upload_type != 4', 'group'=>'id_user'));
		$retail = UserUploadData::model()->findAllByAttributes(array('id_upload_type'=>1));
		$train = UserUploadData::model()->findAllByAttributes(array('id_upload_type'=>2));
		$flight = UserUploadData::model()->findAllByAttributes(array('id_upload_type'=>3));

		$query = "SELECT count(*) as jml, tgl FROM (SELECT
			t.*,a.active,
			MIN( t.begda ) AS tgl 
		FROM
			user_personal_information t JOIN user a ON t.id_user = a.id
		WHERE
			t.id_personal_information = 1 AND a.active = 1
		GROUP BY
			t.id_user) t
			GROUP BY tgl";
		$execute = Yii::app()->db->createCommand($query);
        $listAktif = $execute->queryAll();

        $q = "SELECT count(*) as register,minda FROM (SELECT
			*,
			MIN( begda ) AS minda 
		FROM
			user_personal_information 
		WHERE
			id_personal_information = 1 
		GROUP BY
			id_user) t
			GROUP BY minda";
		$exe = Yii::app()->db->createCommand($q);
        $listRegister = $exe->queryAll();

        $q_upload_all = "SELECT COUNT(DISTINCT id_user) AS jml, CAST(created_date AS DATE) AS tgl FROM user_upload_data GROUP BY CAST(created_date AS DATE)";
        $exe_upload_all = Yii::app()->db->createCommand($q_upload_all);
        $listUploadAll = $exe_upload_all->queryAll();

        $q_retail = "SELECT COUNT(*) AS jml, CAST(created_date AS DATE) AS tgl FROM user_upload_data WHERE id_upload_type = 1 GROUP BY CAST(created_date AS DATE)";
        $exe_retail = Yii::app()->db->createCommand($q_retail);
        $listRetail = $exe_retail->queryAll();

        $q_pesawat = "SELECT COUNT(*) AS jml, CAST(created_date AS DATE) AS tgl FROM user_upload_data WHERE id_upload_type = 3 GROUP BY CAST(created_date AS DATE)";
        $exe_pesawat = Yii::app()->db->createCommand($q_pesawat);
        $listPesawat = $exe_pesawat->queryAll();

        $q_kai = "SELECT COUNT(*) AS jml, CAST(created_date AS DATE) AS tgl FROM user_upload_data WHERE id_upload_type = 2 GROUP BY CAST(created_date AS DATE)";
        $exe_kai = Yii::app()->db->createCommand($q_kai);
        $listKai = $exe_kai->queryAll();

    	$arr_date = [];
    	$arr_jml_tgl_r = [];
    	foreach ($listRegister as $value) {
    		$arr_date[] = $value['minda'];
    		$arr_jml_tgl_r[$value['minda']] = $value['register'];
    	}

    	$arr_jml_tgl = [];
    	foreach ($listAktif as $value) {
    		$arr_jml_tgl[$value['tgl']] = $value['jml'];
    	}

    	$arr_jml_tgl_upload = [];
    	foreach ($listUploadAll as $value) {
    		$arr_jml_tgl_upload[$value['tgl']] = $value['jml'];
    	}

    	$arr_jml_tgl_retail = [];
    	foreach ($listRetail as $value) {
    		$arr_jml_tgl_retail[$value['tgl']] = $value['jml'];
    	}

    	$arr_jml_tgl_kai = [];
    	foreach ($listKai as $value) {
    		$arr_jml_tgl_kai[$value['tgl']] = $value['jml'];
    	}

    	$arr_jml_tgl_pesawat = [];
    	foreach ($listPesawat as $value) {
    		$arr_jml_tgl_pesawat[$value['tgl']] = $value['jml'];
    	}
    	
    	$first_date = min($arr_date);
    	$last_date = max($arr_date);

		$date_start = isset($_REQUEST['start']) ? $_REQUEST['start'] : $first_date ;
		$date_end = isset($_REQUEST['end']) ? $_REQUEST['end'] : $last_date ;

		$date_start = new DateTime($date_start);
		$date_end = new DateTime($date_end);
		$date_end = $date_end->modify( '+1 day' ); 

		$daterange = new DatePeriod($date_start, new DateInterval('P1D'), $date_end);

		$arrdate = [];
		$user_active = [];
		$user_register = [];
		$user_upload_all = [];
		$user_retail = [];
		$user_kai = [];
		$user_pesawat = [];
		$i=0;
		foreach($daterange as $date){
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl)) {
				$user_active[$date->format("Y-m-d")] = $arr_jml_tgl[$date->format("Y-m-d")];
			} else {
				$user_active[$date->format("Y-m-d")] = 0;
			}
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl_r)) {
				$user_register[$date->format("Y-m-d")] = $arr_jml_tgl_r[$date->format("Y-m-d")];
			} else {
				$user_register[$date->format("Y-m-d")] = 0;
			}
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl_upload)) {
				$user_upload_all[$date->format("Y-m-d")] = $arr_jml_tgl_upload[$date->format("Y-m-d")];
			} else {
				$user_upload_all[$date->format("Y-m-d")] = 0;
			}
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl_retail)) {
				$user_retail[$date->format("Y-m-d")] = $arr_jml_tgl_retail[$date->format("Y-m-d")];
			} else {
				$user_retail[$date->format("Y-m-d")] = 0;
			}
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl_kai)) {
				$user_kai[$date->format("Y-m-d")] = $arr_jml_tgl_kai[$date->format("Y-m-d")];
			} else {
				$user_kai[$date->format("Y-m-d")] = 0;
			}
			if (array_key_exists($date->format("Y-m-d"), $arr_jml_tgl_pesawat)) {
				$user_pesawat[$date->format("Y-m-d")] = $arr_jml_tgl_pesawat[$date->format("Y-m-d")];
			} else {
				$user_pesawat[$date->format("Y-m-d")] = 0;
			}
		    $arrdate[$i] = $date->format("Y-m-d");
			$i++;
		}
		$this->render('index',array(
			'retail'=>$retail,
			'train'=>$train,
			'flight'=>$flight,
			'receipt'=>$receipt,
			'user'=>$user,
			'listSosmed'=>$listSosmed,
			'arrdate'=>$arrdate,
			'user_active'=>$user_active,
			'user_register'=>$user_register,
			'user_upload_all'=>$user_upload_all,
			'user_retail'=>$user_retail,
			'user_kai'=>$user_kai,
			'user_pesawat'=>$user_pesawat,
		));
	}
	
	public function actionListFeedback(){
		$model = FaqFeedback::model()->findAll(array('order'=>'created_date DESC'));
		
		$this->render('listfeedback',array(
			'model'=>$model
		));
	}

	public function actionPrintFeedback(){
		
		$model = FaqFeedback::model()->findAll(array('order'=>'created_date DESC'));
		$content = $this->renderPartial("print_feedback",array("model"=>$model),true);
		Yii::app()->request->sendFile("faq_feedback.xls",$content);
	  	$this->redirect(['listFeedback']);
	}
	
	public function actionIndexDetail($id, $id_upload_type = null){
		if($id == 1){
			$model = User::model()->findAll(array('condition'=>'active = 1'));
		}else if($id == 2){
			$crec = new CDbCriteria;
			$crec->alias = 'a';
			$crec->select = 'a.id, d.`value` nama, a.email, a.active';
			$crec->join = 'JOIN access_token b ON b.id_user = a.id
			JOIN social_media c ON c.id = b.id_social_media
			JOIN user_personal_information d ON d.id_user = a.id AND d.id_personal_information = 2';
			$crec->condition = 'NOW() BETWEEN d.begda AND d.endda AND b.datetime IS NOT NULL';
			
			$model = User::model()->findAll($crec);
		}else if($id == 3){
			$crec = new CDbCriteria;
			$crec->alias = 'a';
			$crec->select = 'a.id, b.`value` nama, a.email, a.active';
			$crec->join = 'JOIN user_personal_information b ON b.id_user = a.id AND b.id_personal_information = 2
			JOIN user_upload_data c ON c.id_user = a.id AND c.id_upload_type != 4';
			$crec->group = 'c.id_user';
			
			$model = User::model()->findAll($crec);
		}else if($id == 4 || $id == 5 || $id == 6){
			$crec = new CDbCriteria;
			$crec->alias = 'a';
			$crec->select = 'SUM(
				CASE 
					WHEN b.id_approval = 1 THEN 1 ELSE 0
				END 
			)IN_PROGRESS,
			SUM(
				CASE 
					WHEN b.id_approval = 2 THEN 1 ELSE 0
				END 
			)APPROVED,
			SUM(
				CASE 
					WHEN b.id_approval = 2 THEN 1 ELSE 0
				END 
			)REJECTED';
			$crec->join = 'JOIN user_upload_approval b ON b.id_user_upload_data = a.id';
			$crec->condition = 'a.id_upload_type= :param1';
			$crec->params = array(':param1'=>$id_upload_type);
			
			$model = UserUploadData::model()->find($crec);
		}	
		
		$this->render('indexdetail',array(
			'id'=>$id,
			'model'=>$model
		));
	}	
	
	public function actionReceipt($id){
		$mcrit = new CDbCriteria;
		$mcrit->alias = 'a';
		$mcrit->select = 'a.id, a.image_name, a.image_path, b.id_approval, c.upload_type keterangan';
		$mcrit->join = 'JOIN user_upload_approval b ON b.id_user_upload_data = a.id
		JOIN upload_type c ON c.id = a.id_upload_type';
		$mcrit->condition = 'b.id_approval = :param1';
		$mcrit->params = array(':param1'=>$id);
		if($id == 1){
			$mcrit->order = 'a.id ASC';
		}else{	
			$mcrit->order = 'b.updated_date DESC';
		}
		$model = UserUploadData::model()->findAll($mcrit);
		
		$this->render('receipt', array(
			'id'=>$id,
			'model'=>$model
		));
	}	
	
	public function actionTampilReceipt($id, $id_approval){
		$tampilreceipt = UserUploadData::model()->findByAttributes(array('id'=>$id));
		$exp = explode('/', $tampilreceipt->image_type);
		
		$this->render('tampilreceipt', array(
			'id'=>$id,
			'id_approval'=>$id_approval,
			'tampilreceipt'=>$tampilreceipt,
			'exp'=>$exp
		));
	}	
	
	public function actionReceiptDetail($id, $kategori, $id_approval){
		if($kategori == 'rejected'){
			$keterangan = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$id));;
			if(isset($_POST['UserUploadApproval'])){
				$keterangan->scenario = 'rejected';
				$keterangan->attributes = $_POST['UserUploadApproval'];
				$keterangan->id_approval = '3';
				$keterangan->updated_date = date('Y-m-d H:i:s');
				$keterangan->updated_by = Yii::app()->user->name;
				if($keterangan->save()){
					$this->redirect(array('admin/receipt', 'id'=>$id_approval));
				}	
			}
		}else{

			$upload = UserUploadData::model()->findByPk($id);
			$exp = explode('/', $upload->image_type);
			
			$keterangan = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$id));;
			if(isset($_POST['UserUploadApproval'])){
				$keterangan->scenario = 'rejected';
				$keterangan->attributes = $_POST['UserUploadApproval'];
				$keterangan->id_approval = '3';
				$keterangan->updated_date = date('Y-m-d H:i:s');
				$keterangan->updated_by = Yii::app()->user->name;
				if($keterangan->save()){
					$this->redirect(array('admin/receipt', 'id'=>$id_approval));
				}	
			}
			
			if($upload->id_upload_type == 1){
				$receipt = new ReceiptRetail;
			}else if($upload->id_upload_type == 2){
				$receiptpk = new ReceiptPk;
				$receiptpkka = new ReceiptPkKa;
			}else if($upload->id_upload_type == 3){
				$receiptpk = new ReceiptPk;
				$receiptpkpesawat = new ReceiptPkPesawat;
			}
		}	
		
		
		
		$this->render('receiptdetail',array(
			'id'=>$id,
			'kategori'=>$kategori,
			'id_approval'=>$id_approval,
			'keterangan'=>$keterangan,
			'receipt'=>$receipt,
			'upload'=>$upload,
			'exp'=>$exp,
			'receiptpk'=>$receiptpk,
			'receiptpkka'=>$receiptpkka,
			'receiptpkpesawat'=>$receiptpkpesawat
		));
	}	
	
	public function actionSaveRetail($id){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$nama_toko = $_POST['ReceiptRetail']['nama_toko'];
			$alamat_toko = $_POST['ReceiptRetail']['alamat_toko'];
			$tgl_pembelian = date('Y-m-d', strtotime($_POST['ReceiptRetail']['tgl_pembelian']));
			$jam_pembelian = $_POST['ReceiptRetail']['jam_pembelian'];
			
			$receipt = ReceiptRetail:: model()->findByAttributes(array('nama_toko'=>$nama_toko, 'alamat_toko'=>$alamat_toko, 'tgl_pembelian'=>$tgl_pembelian, 'jam_pembelian'=>$jam_pembelian));
			if(empty($receipt)) {
				$receipt = new ReceiptRetail;
				if(isset($_POST['ReceiptRetail'])){
					$receipt->attributes = $_POST['ReceiptRetail'];
					$receipt->id_user_upload_data = $id;
					if($tgl_pembelian != '1970-01-01'){
						$receipt->tgl_pembelian = $tgl_pembelian;
					}
					$receipt->created_date = date('Y-m-d H:i:s');
					$receipt->created_by = Yii::app()->user->name;
					if($receipt->validate() == true){
						$data['hasil'] = 'success';
					}else{
						$data['hasil'] = CHtml::errorSummary($receipt);
					}
					if($data['hasil'] == 'success'){
						$receipt->save();
					}	
				}
			}else{
				$data['hasil'] = 'notvalid';
				$data['notvalid'] = 'Data Tidak Valid';
			}
				
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionSaveBarang($id, $approval){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			if(isset($_POST['nama_barang'])){
				foreach($_POST['nama_barang'] as $bdx=>$brow){
					$barang = new ReceiptRetailBarang;
					$barang->id_receipt_retail = $id;
					$barang->nama_barang = $brow;
					$barang->jumlah_barang = $_POST['jumlah_barang'][$bdx];
					$barang->harga_barang = $_POST['harga_barang'][$bdx];
					$barang->id_kategori_barang = $_POST['kategori_barang'][$bdx];
					$barang->id_satuan_barang = $_POST['satuan_barang'][$bdx];
					$barang->created_date = date('Y-m-d H:i:s');
					$barang->created_by = Yii::app()->user->name;
					$barang->save();
					
					$receipt = ReceiptRetail::model()->findByPk($id);
					$approvalsave = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$receipt->id_user_upload_data));
					$approvalsave->id_approval = $approval;
					$approvalsave->keterangan = $_POST['keterangan'];
					$approvalsave->updated_date = date('Y-m-d H:i:s');
					$approvalsave->updated_by = Yii::app()->user->name;
					$approvalsave->save();
				}	
				if($_POST['hadiah'] != ''){
					$upload = UserUploadData::model()->findByAttributes(array('id'=>$receipt->id_user_upload_data));
					$user = UserManual::model()->findByAttributes(array('id'=>$upload->id_user));
					if($_POST['hadiah'] == 1){
						$user->point = $user->point + $_POST['hadiahvalue'];
					}else if($_POST['hadiah'] == 2){
						$user->cash = $user->cash + $_POST['hadiahvalue'];
					}
					if($user->save()){
						$wallet = new Wallet;
						$wallet->id_user = $user->id;
						$wallet->datetime = date('Y-m-d H:i:s');
						if($_POST['hadiah'] == 1){
							$wallet->type = 'point';
						}else if($_POST['hadiah'] == 2){
							$wallet->type = 'cash';
						}	
						$wallet->amount = $_POST['hadiahvalue'];
						$wallet->drcr = 'credit';
						$wallet->statement = 'Upload Receipt Retail';
						if($wallet->save()){
							$data['hasil'] = 'success';	
						}	
					}	
				}
				$data['hasil'] = 'success';
			}	
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionSaveReceiptKereta($id, $approval){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$tgl_berangkat = date('Y-m-d', strtotime($_POST['ReceiptPkKa']['tgl_berangkat']));
			$tgl_sampai = date('Y-m-d', strtotime($_POST['ReceiptPkKa']['tgl_sampai']));
			$tgl_pembelian_tiket = date('Y-m-d', strtotime($_POST['ReceiptPkKa']['tgl_pembelian_tiket']));
			if(isset($_POST['ReceiptPkKa'])){
				$model = new ReceiptPkKa;
				$model->attributes = $_POST['ReceiptPkKa'];
				$model->id_receipt_pk = $id;
				if($tgl_berangkat != '1970-01-01'){
					$model->tgl_berangkat = $tgl_berangkat;
				}
				if($tgl_sampai != '1970-01-01'){
					$model->tgl_sampai = $tgl_sampai;
				}
				if($tgl_pembelian_tiket != '1970-01-01'){
					$model->tgl_pembelian_tiket = $tgl_pembelian_tiket;
				}
				if($model->validate() == true){
					$data['hasil'] = 'success';
				}else{
					$data['hasil'] = CHtml::errorSummary($model);
				}
				$model->created_date = date('Y-m-d H:i:s');
				$model->created_by = Yii::app()->user->name;
				if($data['hasil'] == 'success'){
					if($model->save()){
						$receiptpk = ReceiptPk::model()->findByPk($id);
						$approvalsave = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$receiptpk->id_user_upload_data));
						$approvalsave->id_approval = $approval;
						$approvalsave->keterangan = $_POST['keterangan'];
						$approvalsave->updated_date = date('Y-m-d H:i:s');
						$approvalsave->updated_by = Yii::app()->user->name;
						$approvalsave->save();
						
						if($_POST['hadiah'] != ''){
							$upload = UserUploadData::model()->findByAttributes(array('id'=>$receiptpk->id_user_upload_data));
							$user = UserManual::model()->findByAttributes(array('id'=>$upload->id_user));
							if($_POST['hadiah'] == 1){
								$user->point = $user->point + $_POST['hadiahvalue'];
							}else if($_POST['hadiah'] == 2){
								$user->cash = $user->cash + $_POST['hadiahvalue'];
							}
							if($user->save()){
								$wallet = new Wallet;
								$wallet->id_user = $user->id;
								$wallet->datetime = date('Y-m-d H:i:s');
								if($_POST['hadiah'] == 1){
									$wallet->type = 'point';
								}else if($_POST['hadiah'] == 2){
									$wallet->type = 'cash';
								}	
								$wallet->amount = $_POST['hadiahvalue'];
								$wallet->drcr = 'credit';
								$wallet->statement = 'Upload Receipt Kereta Api';
								$wallet->save();
							}	
						}
					}	
				}	
			}	
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}
	
	public function actionSaveReceiptPesawat($id, $approval){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$tgl_berangkat = date('Y-m-d', strtotime($_POST['ReceiptPkPesawat']['tgl_berangkat']));
			$tgl_sampai = date('Y-m-d', strtotime($_POST['ReceiptPkPesawat']['tgl_sampai']));
			$tgl_pembelian_tiket = date('Y-m-d', strtotime($_POST['ReceiptPkPesawat']['tgl_pembelian_tiket']));
			if(isset($_POST['ReceiptPkPesawat'])){
				$model = new ReceiptPkPesawat;
				$model->attributes = $_POST['ReceiptPkPesawat'];
				$model->id_receipt_pk = $id;
				if($tgl_berangkat != '1970-01-01'){
					$model->tgl_berangkat = $tgl_berangkat;
				}
				if($tgl_sampai != '1970-01-01'){
					$model->tgl_sampai = $tgl_sampai;
				}
				if($tgl_pembelian_tiket != '1970-01-01'){
					$model->tgl_pembelian_tiket = $tgl_pembelian_tiket;
				}
				if($model->validate() == true){
					$data['hasil'] = 'success';
				}else{
					$data['hasil'] = CHtml::errorSummary($model);
				}
				$model->created_date = date('Y-m-d H:i:s');
				$model->created_by = Yii::app()->user->name;
				if($data['hasil'] == 'success'){
					if($model->save()){
						$receiptpk = ReceiptPk::model()->findByPk($id);
						$approvalsave = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$receiptpk->id_user_upload_data));
						$approvalsave->id_approval = $approval;
						$approvalsave->keterangan = $_POST['keterangan'];
						$approvalsave->updated_date = date('Y-m-d H:i:s');
						$approvalsave->updated_by = Yii::app()->user->name;
						$approvalsave->save();
						
						if($_POST['hadiah'] != ''){
							$upload = UserUploadData::model()->findByAttributes(array('id'=>$receiptpk->id_user_upload_data));
							$user = UserManual::model()->findByAttributes(array('id'=>$upload->id_user));
							if($_POST['hadiah'] == 1){
								$user->point = $user->point + $_POST['hadiahvalue'];
							}else if($_POST['hadiah'] == 2){
								$user->cash = $user->cash + $_POST['hadiahvalue'];
							}
							if($user->save()){
								$wallet = new Wallet;
								$wallet->id_user = $user->id;
								$wallet->datetime = date('Y-m-d H:i:s');
								if($_POST['hadiah'] == 1){
									$wallet->type = 'point';
								}else if($_POST['hadiah'] == 2){
									$wallet->type = 'cash';
								}	
								$wallet->amount = $_POST['hadiahvalue'];
								$wallet->drcr = 'credit';
								$wallet->statement = 'Upload Receipt Pesawat';
								$wallet->save();
							}	
						}
					}	
				}	
			}	
			
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}
	
	public function actionViewReceipt($id, $id_approval){
		
		$approval = UserUploadApproval::model()->findByAttributes(array('id_user_upload_data'=>$id, 'id_approval'=>$id_approval));
		$tampilreceipt = UserUploadData::model()->findByAttributes(array('id'=>$id));
		$exp = explode('/', $tampilreceipt->image_type);
		
		if($tampilreceipt->id_upload_type == 1){
			$toko = ReceiptRetail::model()->findByAttributes(array('id_user_upload_data'=>$tampilreceipt->id));
			$barang = ReceiptRetailBarang::model()->findAllByAttributes(array('id_receipt_retail'=>$toko->id_retail));
			$wallet = Wallet::model()->findByAttributes(array('id_user'=>$tampilreceipt->id_user, 'statement'=>'Upload Receipt Retail'));
		}else if($tampilreceipt->id_upload_type == 2){
			$kereta = ReceiptPk::model()->findByAttributes(array('id_user_upload_data'=>$tampilreceipt->id));
			$keretadetail = ReceiptPkKa::model()->findByAttributes(array('id_receipt_pk'=>$kereta->id_pk));
			$wallet = Wallet::model()->findByAttributes(array('id_user'=>$tampilreceipt->id_user, 'statement'=>'Upload Receipt Kereta Api'));
		}else if($tampilreceipt->id_upload_type == 3){
			$pesawat = ReceiptPk::model()->findByAttributes(array('id_user_upload_data'=>$tampilreceipt->id));
			$pesawatdetail = ReceiptPkPesawat::model()->findByAttributes(array('id_receipt_pk'=>$pesawat->id_pk));
			$wallet = Wallet::model()->findByAttributes(array('id_user'=>$tampilreceipt->id_user, 'statement'=>'Upload Receipt Pesawat'));
		}
		
		
		$this->render('viewreceipt', array(
			'id'=>$id,
			'id_approval'=>$id_approval,
			'approval'=>$approval,
			'tampilreceipt'=>$tampilreceipt,
			'exp'=>$exp,
			'toko'=>$toko,
			'barang'=>$barang,
			'wallet'=>$wallet,
			'kereta'=>$kereta,
			'keretadetail'=>$keretadetail,
			'pesawat'=>$pesawat,
			'pesawatdetail'=>$pesawatdetail
		));	
	}	
	
	public function actionSavePk($id){
		$data = array();
		if(Yii::app()->request->isAjaxRequest){
			$nama_penumpang = $_POST['ReceiptPk']['nama_penumpang'];
			$kode_booking = $_POST['ReceiptPk']['kode_booking'];
			
			$upload = UserUploadData::model()->findByAttributes(array('id'=>$id));
			$receipt = ReceiptPk:: model()->findByAttributes(array('id_upload_type'=>$upload->id_upload_type, 'nama_penumpang'=>$nama_penumpang, 'kode_booking'=>$kode_booking));
			if(empty($receipt)) {
				$personal = UserPersonalInformation::model()->find(array('condition'=>'id_user = :param1 AND id_personal_information = :param2 AND NOW() BETWEEN begda AND endda', 'params'=>array(':param1'=>$upload->id_user, ':param2'=>2)));
				
				$receipt = new ReceiptPk;
				if(isset($_POST['ReceiptPk'])){
					$receipt->attributes = $_POST['ReceiptPk'];
					$receipt->id_user_upload_data = $id;
					$receipt->id_upload_type = $upload->id_upload_type;
					if($nama_penumpang == $personal->value){
						$receipt->cek_akun = 1;
					}else{
						$receipt->cek_akun = 2;
					}
					$receipt->created_date = date('Y-m-d H:i:s');
					$receipt->created_by = Yii::app()->user->name;
					if($receipt->validate() == true){
						$data['hasil'] = 'success';
					}else{
						$data['hasil'] = CHtml::errorSummary($receipt);
					}
					if($data['hasil'] == 'success'){
						$receipt->save();
					}	
				}
			}else{
				$data['hasil'] = 'notvalid';
				$data['notvalid'] = 'Data Tidak Valid';
			}
				
			echo CJSON::encode($data);
			Yii::app()->end();
		}	
	}	
	
	public function actionViewPdf($id){
		$tampilreceipt = UserUploadData::model()->findByAttributes(array('id'=>$id));
		
		$path = 'F:/wamp64/www/bagidata'.$tampilreceipt->image_path;
		header("Content-type:".$tampilreceipt->image_type); 
		// header('Content-Disposition: attachment; filename="'.basename($path).'"');
		header('Content-Length: ' . filesize($path));
		readfile($path);
		Yii::app()->end();
	}	
	
	public function actionShowReferral(){
		$this->layout = "mainguest";
		$all_kode = Referral::model()->findAll();
		$this->render('viewreferral', array(
			'all_kode'=>$all_kode,
		));	
	}

	public function actionShowRef(){
		$this->layout = "mainguest";
		$jml_user = count(User::model()->findAllByAttributes(array('active'=>1,'ref_code'=>'dramaojol')));
		$this->render('viewref', array(
			'jml_user'=>$jml_user,
		));	
	}

	public function actionShowCupstuwerd(){
		$this->layout = "mainguest";
		$jml_user = count(User::model()->findAllByAttributes(array('active'=>1,'ref_code'=>'cupstuwerd')));
		$this->render('viewref', array(
			'jml_user'=>$jml_user,
		));	
	}
}

?>
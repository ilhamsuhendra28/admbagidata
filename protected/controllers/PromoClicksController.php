<?php

class PromoClicksController extends Controller
{

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','detail','prints','printsdetail'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	
	public function actionIndex()
	{
		$model = Promo::model()->findAll();
		$this->render('index',array(
			'model'=>$model,
		));
	}

	
		public function actionDetail($id_promo)
		{
			$id = $id_promo;
			$model = UserPromo::model()->findAllByAttributes(array('id_promo'=>$id_promo),array('group'=>'id_user'));

				$this->render('detail',array(
			'model'=>$model,
			'id'=>$id,
		));
	}	

		public function actionPrints(){
		
			$model = Promo::model()->findAll();
			$content = $this->renderPartial("printindex",array("model"=>$model),true);
			Yii::app()->request->sendFile("buy_promo.xls",$content);
		  	$this->redirect(['index']);
	}
		public function actionPrintsDetail($id_promo){
			$model = UserPromo::model()->findAllByAttributes(array('id_promo'=>$id_promo),array('group'=>'id_user'));
			$content = $this->renderPartial("printdetail",array("model"=>$model,"id"=>$id_promo),true);
			Yii::app()->request->sendFile("detail_user_id_promo_".$id_promo.".xls",$content);
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}




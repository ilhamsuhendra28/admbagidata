<?php

class PromoController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'approval', 'validasi', 'indexdetail', 'receipt', 'tampilreceipt', 'receiptdetail', 'saveretail', 'savebarang', 'savereceiptkereta', 'savereceiptpesawat', 'viewreceipt', 'savepk', 'viewpdf','listFeedback'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('showReferral'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex($id)
	{
        $listPromo = Promo::model()->findAllByAttributes(['approval'=>$id]);
		$this->render('index',[
            'listPromo'=>$listPromo,
            'id'=>$id
            ]);
	}
    
    public function actionValidasi($id)
    {
        $promo = Promo::model()->findByPk($id);
        if ($_POST){
            $promo->attributes = $_POST['Promo'];
//            var_dump($promo);exit;
            if ($promo->approval == 0){
                $promo->addError ('approval', 'Approval is Empty!');
            } else {
                $promo->save();
                $this->redirect(array('promo/index','id'=>'0'));
            }
        }
        if ($promo->approval == '0'){
            $this->render('validasi',[
                'promo'=>$promo,
            ]);
        } else {
            $this->render('detail',[
                'promo'=>$promo,
            ]);
        }
    }
    
    public function actionApproval($id)
    {
        $status = Yii::app()->request->getParam('approval');
        if ($status == null)
            $status = 0;
        $pesan = Yii::app()->request->getParam('approval_message');
        $promo = Promo::model()->findByPk($id);
//        var_dump($promo);exit;
        $promo->approval = $status;
        $promo->approval_message = $pesan;
        $promo->save();
        $this->redirect(array('promo/index','id'=>'0'));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
<?php

class CampaignController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'approval', 'validasi', 'indexdetail', 'receipt', 'tampilreceipt', 'receiptdetail', 'saveretail', 'savebarang', 'savereceiptkereta', 'savereceiptpesawat', 'viewreceipt', 'savepk', 'viewpdf','listFeedback'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('showReferral'),
				'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	public function actionIndex($id)
	{
        $listCampaign = Campaign::model()->findAllByAttributes(['approval'=>$id]);
		$this->render('index',[
            'listCampaign'=>$listCampaign,
            'id'=>$id
            ]);
	}
    
    public function actionValidasi($id)
    {
        $campaign = Campaign::model()->findByPk($id);
        if ($_POST){
            $campaign->attributes = $_POST['Campaign'];
//            var_dump($campaign);exit;
            if ($campaign->approval == 0){
                $campaign->addError ('approval', 'Approval is Empty!');
            } else {
                $campaign->save();
                $this->redirect(array('campaign/index','id'=>'0'));
            }
        }
        if ($campaign->approval == '0'){
            $this->render('validasi',[
                'campaign'=>$campaign,
            ]);
        } else {
            $this->render('detail',[
                'campaign'=>$campaign,
            ]);
        }
    }
    
    public function actionApproval($id)
    {
        $status = Yii::app()->request->getParam('approval');
        $pesan = Yii::app()->request->getParam('approval_message');
        $campaign = Campaign::model()->findByPk($id);
//        var_dump($campaign);exit;
        $campaign->approval = $status;
        $campaign->approval_message = $pesan;
        $campaign->save();
        $this->redirect(array('campaign/index','id'=>'0'));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
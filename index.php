<?php
// error_reporting(0);
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/*
if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
    $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $redirect);
    exit();
}
*/

// change the following paths if necessary
$yii=dirname(__FILE__).'/yii-1.1.19/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

date_default_timezone_set("Asia/Jakarta");

require_once($yii);
Yii::createWebApplication($config)->run();
